package ui.smoke.testcases.payMonitor;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToEnable;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.PayMonitor_Pages.IMercer_HomePage;
import pages.PayMonitor_Pages.RegionSelectionPage;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

public class TC1_LoginToPayMonitor extends InitTests {

	public TC1_LoginToPayMonitor(String appName) {
		super(appName);
		// TODO Auto-generated constructor stub
	}

	@Test(enabled = true)
	public void login_payMonitor() throws Exception {
		try {
			test = reports.createTest("Logging in to Pay Monitor");
			test.assignCategory("smoke");
			
			TC1_LoginToPayMonitor login = new TC1_LoginToPayMonitor("PayMonitor");

			//Driver.deleteCookies(driver);
			//Thread.sleep(5000);
			System.out.println("Logging in to application");
			initWebDriver(BASEURL, "CHROME", "latest","","local", test, "");
			System.out.println("BaseURL is: " + BASEURL);

			RegionSelectionPage regionSelectionPage = new RegionSelectionPage();
			regionSelectionPage.clickRegionLink("Asia");
			Thread.sleep(5000);

			IMercer_HomePage iHome = new IMercer_HomePage();
			iHome.login_hover();
			iHome.login_imercer(USERNAME, PASSWORD);
			iHome.login_imercer_ReferenceError(USERNAME, PASSWORD);
			
			iHome.clickCookieBtn();
			
			assertTrue(Driver.isElementExisting(driver,iHome.Paymonitor_link, 50), "Paymonitor Link is present.",test);
			iHome.clickPayMonitorLink();
			
			assertTrue(Driver.isElementExisting(driver,iHome.country_dropdown, 50), "User has logged in Paymonitor successfully.",test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			IMercer_HomePage iHome = new IMercer_HomePage();
			Driver.waitForElementToDisplay(iHome.exitPM_link);
			iHome.clickPayMonitorLink();
			hoverOverElement(driver,iHome.account_Link);
			clickElement(iHome.account_Link);
			waitForElementToEnable(iHome.logout);
			// hoverOverElement(childElement);
			clickElement(iHome.logout);
			Driver.deleteCookies(driver);
			driver.quit();

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			IMercer_HomePage iHome = new IMercer_HomePage();
			Driver.waitForElementToDisplay(iHome.exitPM_link);
			iHome.clickPayMonitorLink();
			hoverOverElement(driver,iHome.account_Link);
			clickElement(iHome.account_Link);
			waitForElementToEnable(iHome.logout);
			// hoverOverElement(childElement);
			clickElement(iHome.logout);
			Driver.deleteCookies(driver);
			driver.quit();
		} finally {

			reports.flush();
			
		}
	}

	
}
