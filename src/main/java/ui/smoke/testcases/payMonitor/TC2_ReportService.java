package ui.smoke.testcases.payMonitor;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToEnable;
import static utilities.InitTests.BROWSER_TYPE;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.fail;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.PayMonitor_Pages.IMercer_HomePage;
import pages.PayMonitor_Pages.ReportServices_Pages;

public class TC2_ReportService {
	
	@Test(enabled=true)
	public void ReportService() throws Exception {
		try {

			test = reports.createTest("Verify Reports");
			test.assignCategory("smoke");
			ReportServices_Pages rs = new ReportServices_Pages();

			rs.selectCountryFromDropdown("Belgium");
			rs.selectYearFromDropdown("2003");
			assertTrue(Driver.isElementExisting(driver,rs.customize_link, 20), "Report Template is present.",test);
			rs.loadAndClickCustomizeLink();
			Thread.sleep(5000);
			assertTrue(Driver.isElementExisting(driver,rs.run_link, 20), "Run link is present.",test);
			rs.loadAndClickRunLink();
			rs.loadAndClickBtnLink();
			assertTrue(Driver.isElementExisting(driver,rs.next_btn, 20), "Select Positions page is present.",test);
			rs.scrollAndClickNextBtn();
			rs.pageLoadWait();
			assertTrue(Driver.isElementExisting(driver,rs.selectMarketList, 20), "Select Market page is present.",test);
			String handle = rs.selectOptionFromDropdown();
			rs.switchWindow(handle);
			rs.scrollAndClickNextBtn();
			assertTrue(Driver.isElementExisting(driver,rs.generateReport_link, 20),"Generate report link is present",test);
			rs.clickGenerateReport();
			rs.enterReportTitle();
			rs.enterEmail();
			rs.verifyCheckbox();
			rs.scrollAndClickSubmit();
			rs.clickMyReports();
			//rs.clickViewLink();
			
			//Since the link is not working correctly in the QA link the code for verifying generated report is yet to developed.
			
			rs.clickPayMonitorLink();

		} 
		catch (Error e) {
			e.printStackTrace();
			IMercer_HomePage iHome = new IMercer_HomePage();
			fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Report generation failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			Driver.waitForElementToDisplay(iHome.exitPM_link);
			iHome.clickPayMonitorLink();
			hoverOverElement(driver,iHome.account_Link);
			clickElement(iHome.account_Link);
			waitForElementToEnable(iHome.logout);
			// hoverOverElement(childElement);
			clickElement(iHome.logout);
			Driver.deleteCookies(driver);
			driver.quit();

		} catch (Exception e) {
			e.printStackTrace();

			fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Report generation failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));
			IMercer_HomePage iHome = new IMercer_HomePage();
			Driver.waitForElementToDisplay(iHome.exitPM_link);
			clickElement(iHome.exitPM_link);
			hoverOverElement(driver,iHome.account_Link);
			clickElement(iHome.account_Link);
			waitForElementToEnable(iHome.logout);
			// hoverOverElement(childElement);
			clickElement(iHome.logout);
			Driver.deleteCookies(driver);
			driver.quit();

		}
		finally {
			
		}

	}
	@AfterSuite
	public void tearDown()
	{
		reports.flush();
		killBrowserExe("CHROME");
	}

}
