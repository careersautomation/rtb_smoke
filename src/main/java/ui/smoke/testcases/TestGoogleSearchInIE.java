package ui.smoke.testcases;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;


import pages.DashBoardPage;
import pages.GoogleHomePage;
import utilities.InitTests;
import verify.SoftAssertions;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

public class TestGoogleSearchInIE extends InitTests {
	DashBoardPage home;

	@Test( enabled = true)
	public void testSearchInIE() throws Exception {
		Driver driverFact=new Driver();

		WebDriver driver = null;
		ExtentTest test=null;
		try {
			
			 test = reports.createTest("testSearchInIE");
			test.assignCategory("smoke");
		 driver=driverFact.initWebDriver("https://www.google.co.in","IE","","","local",test,"");
			GoogleHomePage ghomeIe=new GoogleHomePage(driver);
			ghomeIe.search("selenium");
			waitForElementToDisplay(ghomeIe.getFirstLnk());
			verifyElementTextContains(ghomeIe.getFirstLnk(), "selenium",test);
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();

			

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();


		} 
		finally
		{
			reports.flush();
			driver.close();

		}
	}

}
