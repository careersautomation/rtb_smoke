package ui.smoke.testcases.GHRM;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.switchToWindow;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyTableContent;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.GHRM_Pages.GHRMCalculatorsPage;
import pages.GHRM_Pages.GHRMHeaderPage;
import pages.GHRM_Pages.HomePage;
import pages.GHRM_Pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC3i_GHMRVerification extends InitTests{
	
	public TC3i_GHMRVerification(String appname) {
		super(appname);
		// TODO Auto-generated constructor stub
	}

	@BeforeMethod()
	public void initializeBrowser() throws Exception {
		test = reports.createTest("GHMRVerifciation_Login");
		test.assignCategory("smoke");
		softAssert = new SoftAssert();
		TC3h_GHMRVerification test1 = new TC3h_GHMRVerification("GHMRV");
		initWebDriver(BASEURL, "chrome", "", "", "local", test, "");
	}

	@Test(priority = 1, enabled = true)
	public void HPTCalculatorTest() throws Exception {
		try {
			HomePage homePage = new HomePage();
			homePage.clickAsiaLink();
			homePage.clickSigninBtn();
			LoginPage loginPage = new LoginPage();
			loginPage.loginToApp();
			verifyElementTextContains(homePage.welWithNameElm, USERNAME.replace("@mercer.com", ""), test);
			homePage.clickGlobalHRMonitorLink();
			GHRMHeaderPage ghrmHeaderPage = new GHRMHeaderPage();
			ghrmHeaderPage.clickCalculatorsLink();
			GHRMCalculatorsPage ghrmCalPage = new GHRMCalculatorsPage();
			ghrmCalPage.clickCalculatorsLink("Expatriate Compensation");
			ghrmCalPage.clickCreateOwnTableBtn();
			ghrmCalPage.enterECCDetails("5000","INDIA", "BENGALURU", "UNITED STATES", "CHICAGO, IL");
			ghrmCalPage.clickCalculateBtn();
			switchToWindow("Expatriate Compensation Interim Results Page");
			verifyTableContent(ghrmCalPage.ECCResultTbl, "ECC Result Table", "BENGALURU~CHICAGO", test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	
	}

	@AfterSuite
	public void kill() {
		driver.quit();
		killBrowserExe("chrome");
	}
}
