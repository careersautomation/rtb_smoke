package ui.smoke.testcases.GHRM;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.switchToWindow;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;
import org.openqa.selenium.By;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.GHRM_Pages.GHMRHealthPage;
import pages.GHRM_Pages.HomePage;
import pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;


public class TC1_GHMRVerification extends InitTests{

	public TC1_GHMRVerification(String appname) {
		super(appname);
		// TODO Auto-generated constructor stub
	}

	@BeforeMethod()
	public void initializeBrowser() throws Exception {
		test = reports.createTest("GHMRVerifciation_Health");
		test.assignCategory("smoke");
		softAssert = new SoftAssert();
		TC1_GHMRVerification tes = new TC1_GHMRVerification("GHMVHealth");
		initWebDriver(BASEURL, "IE", "", "", "local", test, "");

	}

	@Test(priority = 1, enabled = true)
	public void testSearchJob() throws Exception {
		try {
			GHMRHealthPage ghmrHealthPage = new GHMRHealthPage();
			verifyElementTextContains(ghmrHealthPage.GHMRHealthTitleElm, "Global HRMonitor Health Check", test);
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

	@AfterSuite
	public void kill() {
		driver.quit();
		killBrowserExe("chrome");
	}
}


