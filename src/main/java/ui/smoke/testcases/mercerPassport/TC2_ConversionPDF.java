package ui.smoke.testcases.mercerPassport;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextIgnoreCase;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.MercerPassport_Pages.HeaderPage;
import pages.MercerPassport_Pages.UnitConversionPage;
import verify.SoftAssertions;

public class TC2_ConversionPDF {
		
	
	@Test(enabled = true)
	public void unitConversion_MercerPassport()  throws Exception {
		try {
			test = reports.createTest("Unit conversion information");
			test.assignCategory("smoke");
			String oldHandle = driver.getWindowHandle();
			
			HeaderPage headerPage = new HeaderPage();
			headerPage.navigateToHome();
			headerPage.navigatingViaMenu(HeaderPage.setlingIn_Menu, HeaderPage.conversion_Button);
		
			
			UnitConversionPage conversionPage = new UnitConversionPage();
			verifyElementTextIgnoreCase(UnitConversionPage.conversionPagetitle, "General Conversion Tables",test);
			
		
			clickElement(UnitConversionPage.printVersion_Button);

			driver.switchTo().window(oldHandle);
		
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("testSearchJob()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} 
	}
}
