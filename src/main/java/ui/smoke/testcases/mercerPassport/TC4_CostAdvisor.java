package ui.smoke.testcases.mercerPassport;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.MercerPassport_Pages.CostAdvisorPage;
import pages.MercerPassport_Pages.HeaderPage;
import verify.SoftAssertions;

public class TC4_CostAdvisor {
	
	@Test(enabled = true)
	public void costAdvisor_MercerPassport() throws Exception {
		try {
			test = reports.createTest("Check the cost of some essential products in another currency");
			test.assignCategory("smoke");


			HeaderPage headerPage = new HeaderPage();
			headerPage.navigateToHome();
			headerPage.navigatingViaMenu(HeaderPage.setlingIn_Menu, HeaderPage.monerAndBank_Button,HeaderPage.costAdvisor_Button);
			
			String oldHandle = driver.getWindowHandle();
			
			CostAdvisorPage costAdvisorPage = new CostAdvisorPage();
			waitForElementToDisplay(CostAdvisorPage.costAdvisor_Title);
			
			costAdvisorPage.selectCountry("Canada");
			
			costAdvisorPage.selectOptions(CostAdvisorPage.option1);
			costAdvisorPage.selectOptions(CostAdvisorPage.option2);
			
			clickElement(CostAdvisorPage.calculate_Button);
			
			switchToWindow("CostAdvisor Result");
			
			SoftAssertions.verifyElementTextIgnoreCase(CostAdvisorPage.headerCityResults, "Mercer - City Results", test);
		
			driver.switchTo().window(oldHandle);
						
			// read excel function remaining

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Calculation failed due to error", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Calculation failed due to error", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} 
	}
}
