package ui.smoke.testcases.mercerPassport;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.MercerPassport_Pages.HeaderPage;
import pages.MercerPassport_Pages.HomePage;
import verify.SoftAssertions;


public class TC3_CountryTypeHeader {

	@Test(enabled = true)
	public void countryType_MercerPassport() throws Exception {
		try {

			test = reports.createTest("countryType_MercerPassport");
			test.assignCategory("smoke");

			String[] expected_CRG_Header = { "Country Facts", "Before You Go", "Travel", "Settling In",
					"Health & Safety", "Business", "Family Corner", "Survey" };
			String[] expected_MSG_Header = { "Country Facts", "Before You Go", "Travel", "Settling In",
					"Health & Safety", "Family Corner", "Survey" };
		

			HeaderPage headerPage = new HeaderPage();
			headerPage.navigateToHome();

			HomePage homePage = new HomePage();
			homePage.selectCountry("Canada");
			homePage.clickCountryMarket();

			List<WebElement> actualHeaderCRG = homePage.fetchHeaderMenuElement();
			SoftAssertions.verifyElementListTextIgnoreCase(actualHeaderCRG, expected_CRG_Header, test);

			headerPage.navigateToHome();

			homePage.selectCountry("Bahamas");
			homePage.clickCountryMarket();
			List<WebElement> actualHeaderMSG = homePage.fetchHeaderMenuElement();

			SoftAssertions.verifyElementListTextIgnoreCase(actualHeaderMSG, expected_MSG_Header, test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Header content is mismatched", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Header content is mismatched", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		} 
	}
}
