package ui.smoke.testcases.mercerPassport;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.MercerPassport_Pages.HeaderPage;
import pages.MercerPassport_Pages.LoginPage;
import verify.SoftAssertions;

public class TC5_LogoutFromMercerPassport {
	
	
	@Test(enabled=true)
	public void login_mercerPassport() throws Exception{
		try {
			test = reports.createTest("Logging out from Mercer Passport");
			test.assignCategory("smoke");
			
			HeaderPage headerPage = new HeaderPage();
			headerPage.mp_logout();

			LoginPage loginPage = new LoginPage();
			SoftAssertions.verifyElementTextContains(LoginPage.welcomeMessage,"Please enter your email address and password below to access MercerPassport.", test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Logging in failed due to error", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Logging in failed due to exception", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		} 

}
}
