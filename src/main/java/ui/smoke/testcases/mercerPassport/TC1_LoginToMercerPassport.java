package ui.smoke.testcases.mercerPassport;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.MercerPassport_Pages.HeaderPage;
import pages.MercerPassport_Pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_LoginToMercerPassport extends InitTests {

	public TC1_LoginToMercerPassport(String appName) {
		super(appName);
		// TODO Auto-generated constructor stub
	}

	@Test(enabled = true)
	public void login_mercerPassport() throws Exception {
		try {
			test = reports.createTest("Logging in to Mercer Passport");
			test.assignCategory("smoke");

			TC1_LoginToMercerPassport login = new TC1_LoginToMercerPassport("MercerPassport");

			System.out.println("Logging in to application");
//			String url,String browserType,String browserVersion,String platform,String executionEnv,ExtentTest test,String nodeUrl
			initWebDriver(BASEURL, "IE", "latest","","local", test, "");
			System.out.println("BaseURL is: " + BASEURL);

			LoginPage loginPage = new LoginPage();
			loginPage.mp_login(USERNAME, PASSWORD);

			HeaderPage headerPage = new HeaderPage();
			SoftAssertions.assertTrue(isElementExisting(driver,HeaderPage.setlingIn_Menu, 5), "User has logged in to application",test);
			

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
	@AfterSuite
	public void killDriver() {
		reports.flush();
		driver.close();
		killBrowserExe(BROWSER_TYPE);
		
	}
}
