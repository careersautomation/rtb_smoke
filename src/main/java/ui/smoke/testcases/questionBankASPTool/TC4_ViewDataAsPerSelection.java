package ui.smoke.testcases.questionBankASPTool;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.questionBankASPTool_Pages.SideMenuPage;
import pages.questionBankASPTool_Pages.ViewDataPage;
import verify.SoftAssertions;

public class TC4_ViewDataAsPerSelection {

	@Test(enabled = true, priority = 1)
	public void selectCompaniesfucntionality() throws Exception {
		try {
			test = reports.createTest("checking if selected data is applied");
			test.assignCategory("smoke");

			SideMenuPage sideMenuPage = new SideMenuPage();
			SoftAssertions.assertTrue(isElementExisting(driver,SideMenuPage.veiwData_link, 5), "User has logged in to application",test);
			
			sideMenuPage.navigateViaSideMenu(SideMenuPage.veiwData_link);
			
			ViewDataPage viewDataPage= new ViewDataPage();
			SoftAssertions.assertTrue(isElementExisting(driver,ViewDataPage.viewDataPage_header, 5), "Reached view data page",test);
			int i = 0;
			for(WebElement e : ViewDataPage.benefits_dropdownOption) {
				SoftAssertions.verifyElementContains(e, TC3_SelectBenefits.selectedBenefits.get(i),"Option is correct as selected",test);
				i++;
			}
			
			int j = 0;
			int k = 0;
			for(WebElement e : ViewDataPage.company_dropdownOption) {
				if(k == 0) {
					k++;
				}
				else {
				SoftAssertions.verifyElementContains(e, TC2_SelectCompanies.selectedCompanies.get(j),"Option is correct as selected",test);
				j++;
				}
			}
			
			String selectedBenefit = "2.1 - "+TC3_SelectBenefits.selectedBenefits.get(1);
			String benefitToBeSelected ="39 : Questionnaire Version 39";
			viewDataPage.selectFromDropdown(ViewDataPage.benefits_dropdown, selectedBenefit);
			viewDataPage.selectFromDropdown(ViewDataPage.company_dropdown, "All");
			viewDataPage.selectRadioButton(ViewDataPage.benefitWise_radiobutton);
			viewDataPage.selectFromDropdown(ViewDataPage.questionnaireVersion_dropdown, benefitToBeSelected);
			viewDataPage.selectRadioButton(ViewDataPage.submissionData_radiobutton);
			viewDataPage.retrievedata(ViewDataPage.retrieveData_button);
			
			SoftAssertions.assertTrue(isElementExisting(driver,ViewDataPage.viewDataTable, 5), "Data table viewed",test);
						
		
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Selected company and benefit data is not correctly added", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Selected company and benefit data is not correctly added", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
	
	
	
}


