package ui.smoke.testcases.questionBankASPTool;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.questionBankASPTool_Pages.HomePage;
import pages.questionBankASPTool_Pages.LoginPage;
import pages.questionBankASPTool_Pages.SideMenuPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_LoginToQuestionBankASP extends InitTests{

	
	public TC1_LoginToQuestionBankASP(String appName) {
		super(appName);
		// TODO Auto-generated constructor stub
	}

	@Test(enabled = true)
	public void login_QuestionBankASP() throws Exception {
		try {
			test = reports.createTest("Logging in to Question Bank tool");
			test.assignCategory("smoke");

			TC1_LoginToQuestionBankASP login = new TC1_LoginToQuestionBankASP("questionBankASP");

			System.out.println("Logging in to application");
//			String url,String browserType,String browserVersion,String platform,String executionEnv,ExtentTest test,String nodeUrl
			initWebDriver(BASEURL, "IE", "latest", "", "local", test, "");
			System.out.println("BaseURL is: " + BASEURL);

			LoginPage loginPage = new LoginPage();
			loginPage.qb_login(USERNAME, PASSWORD);

			
			SideMenuPage sideMenuPage = new SideMenuPage();
			HomePage homePage = new HomePage();
//			SoftAssertions.assertTrue(isElementExisting(SideMenuPage.logout_link, 5), "User has logged in to application");
			SoftAssertions.assertTrue(isElementExisting(driver,HomePage.welcome_message, 5), "User has logged in to application", test);
			
	
		
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
	
	@AfterSuite
	public void killDriver() {
		reports.flush();
		driver.close();
		killBrowserExe(BROWSER_TYPE);
		
	}
	
	
}
