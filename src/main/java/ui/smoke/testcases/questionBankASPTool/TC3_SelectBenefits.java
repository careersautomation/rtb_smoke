package ui.smoke.testcases.questionBankASPTool;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.verifyCheckBox;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.questionBankASPTool_Pages.BenefitsPage;
import pages.questionBankASPTool_Pages.SideMenuPage;
import verify.SoftAssertions;

public class TC3_SelectBenefits {
	
	public static List <String> selectedBenefits;
	public static int benefitCount = 6;
	
	@Test(enabled = true, priority = 1)
	public void selectCompanies() throws Exception {
		try {
			test = reports.createTest("selecting Benefits for retrieving data further");
			test.assignCategory("smoke");
		
			SideMenuPage sideMenuPage = new SideMenuPage();
			SoftAssertions.assertTrue(isElementExisting(driver, SideMenuPage.selectBenefits_link, 5), "User can access side menu", test);
			
			sideMenuPage.navigateViaSideMenu(SideMenuPage.selectBenefits_link);
			
			BenefitsPage benefitPage = new BenefitsPage();
			SoftAssertions.assertTrue(isElementExisting(driver, BenefitsPage.Questionnaire_dropdown, 5), "User reached select benefits page",test);
			
			benefitPage.selectFromDropdown(BenefitsPage.Questionnaire_dropdown, "All");
			benefitPage.selectFromDropdown(BenefitsPage.country_dropdown, "Australia");
			
			benefitPage.checkAll();
			System.out.println("All checkboxes are selected");
			
			for(WebElement e : BenefitsPage.selectBenefits_checkbox) {
				SoftAssertions.assertTrue(verifyCheckBox(e), "Checkbox is checked", test);
				
			}
			
			benefitPage.uncheckAll();
			
			System.out.println("All checkboxes are unchecked");
			 
			for(WebElement e : BenefitsPage.selectBenefits_checkbox) {
				SoftAssertions.assertFalse(verifyCheckBox(e), "Checkbox is unchecked", test);
			}
			
			
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test); 
			ATUReports.add("Check All/ Unchck All on benefits not working", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Check All/ Unchck All on benefits not working", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
	
	@Test(enabled = true, priority = 2)
	public void selectBenefits() throws Exception {
		try {
			test = reports.createTest("selecting benefits for retrieving data further");
			test.assignCategory("smoke");
		
			BenefitsPage selectBenefitsPage = new BenefitsPage();
			SoftAssertions.assertTrue(isElementExisting(driver, BenefitsPage.Questionnaire_dropdown, 5), "User reached select benefits page", test);
			
			selectedBenefits = selectBenefitsPage.selectCheckbox(benefitCount);
			System.out.println("Benefits selected");
			System.out.println(selectedBenefits);
			
			selectBenefitsPage.clickSelect();
			
			WebElement ele_checkbox;
			WebElement ele_benefitName;
			
			int i = 1;

			for (i = 1; i<benefitCount; i++) {
				
				ele_checkbox = selectBenefitsPage.fetchLocators(i, BenefitsPage.checkbox_loc);
				ele_benefitName = selectBenefitsPage.fetchLocators(i, BenefitsPage.benefitsName_loc);
				
				if(verifyCheckBox(ele_checkbox)) {
					SoftAssertions.verifyElementTextIgnoreCase(ele_benefitName, selectedBenefits.get(i-1), test);
				}
				
			}
			
			
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test); 
			ATUReports.add("Selecting benefits failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Selecting benefits failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
}
