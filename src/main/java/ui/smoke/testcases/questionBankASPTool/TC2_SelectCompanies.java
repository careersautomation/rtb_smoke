package ui.smoke.testcases.questionBankASPTool;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.verifyCheckBox;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.questionBankASPTool_Pages.SelectCompaniesPage;
import pages.questionBankASPTool_Pages.SideMenuPage;
import verify.SoftAssertions;

public class TC2_SelectCompanies {
	
	public static List <String> selectedCompanies ;
	public static int companyCount = 6;

	@Test(enabled = true, priority = 1)
	public void selectCompaniesfucntionality() throws Exception {
		try {
			test = reports.createTest("checking if all the buttons are working as expeceted");
			test.assignCategory("smoke");
			System.out.println("TC2");
			SideMenuPage sideMenuPage = new SideMenuPage();
			SoftAssertions.assertTrue(isElementExisting(driver,SideMenuPage.selectCompanies_link, 5), "User has logged in to application",test);
			
			sideMenuPage.navigateViaSideMenu(SideMenuPage.selectCompanies_link);
			
			SelectCompaniesPage selectCompaniesPage = new SelectCompaniesPage();
			SoftAssertions.assertTrue(isElementExisting(driver,SelectCompaniesPage.welcome_message, 5), "Reached select companies page",test);
			
			selectCompaniesPage.selectFromDropdown(SelectCompaniesPage.surveyCycleName_dropdown, "2015");
			selectCompaniesPage.selectFromDropdown(SelectCompaniesPage.coutryName_dropdown, "Australia");
			
			selectCompaniesPage.checkAll();
			System.out.println("All checkboxes are selected"+ SelectCompaniesPage.selectCompany_checkbox.size());
			
			for(WebElement e : SelectCompaniesPage.selectCompany_checkbox) {
				SoftAssertions.assertTrue(verifyCheckBox(e), "Checkbox is checked",test);
				
			}
			
			selectCompaniesPage.uncheckAll();
			
			System.out.println("All checkboxes are unchecked");
			 
			for(WebElement e : SelectCompaniesPage.selectCompany_checkbox) {
				SoftAssertions.assertFalse(verifyCheckBox(e), "Checkbox is unchecked", test);
			}
			
		
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Check all/uncheck All not working", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Check all/uncheck All not working", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
	
	@Test(enabled = true, priority = 2)
	public void selectCompanies() throws Exception {
		try {
			test = reports.createTest("selecting companies for retrieving data further");
			test.assignCategory("smoke");
		
			SelectCompaniesPage selectCompaniesPage = new SelectCompaniesPage();
			SoftAssertions.assertTrue(isElementExisting(driver, SelectCompaniesPage.welcome_message, 5), "Reached select companies page",test);
			
			selectedCompanies = selectCompaniesPage.selectCheckbox(companyCount);
			System.out.println("Companies selected");
			System.out.println(selectedCompanies);
			
			selectCompaniesPage.clickSelect();
			
			WebElement ele_checkbox;
			WebElement ele_companyName;
			
			int i = 1;

			for (i = 1; i<companyCount; i++) {
				
				ele_checkbox = selectCompaniesPage.fetchLocators(i, SelectCompaniesPage.checkbox_loc);
				ele_companyName = selectCompaniesPage.fetchLocators(i, SelectCompaniesPage.companyName_loc);
				
				if(verifyCheckBox(ele_checkbox)) {
					SoftAssertions.verifyElementTextIgnoreCase( ele_companyName, selectedCompanies.get(i-1), test);
				}
				
			}
			
			
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),test); 
			ATUReports.add("Selecting companies failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Selecting companies failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}
	
}
