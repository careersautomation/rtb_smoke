package ui.smoke.testcases.questionBankASPTool;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.questionBankASPTool_Pages.ReportsPage;
import pages.questionBankASPTool_Pages.SideMenuPage;
import verify.SoftAssertions;

public class TC5_GetReports {
	
	@Test(enabled = false, priority = 1)
	public void selectCompaniesfucntionality() throws Exception {
		try {
			test = reports.createTest("checking if selected data is applied");
			test.assignCategory("smoke");

			SideMenuPage sideMenuPage = new SideMenuPage();
			SoftAssertions.assertTrue(isElementExisting(driver,SideMenuPage.reports_link, 5), "User has logged in to application",test);
			
			sideMenuPage.navigateViaSideMenu(SideMenuPage.reports_link);
			ReportsPage reportsPage = new ReportsPage();
			SoftAssertions.assertTrue(isElementExisting(driver,ReportsPage.reportsPageHeader, 5), "User reached reports page",test);
			
			reportsPage.selectFromDropdown(ReportsPage.selectReport_dropdown, "PC 2 - Peer Group - Detailed Benefits Benchmark");
			reportsPage.selectFromDropdown(ReportsPage.selectCompany_dropdown, "BD - 2014 - Dell Bangladesh");
			reportsPage.selectFromDropdown(ReportsPage.selectQuestionnaire_dropdown, "28 : Vietnam - 2010");
			reportsPage.getReports();
			
			// since the reports functionality has some issue this part could not be automated
			
						
		
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Report generation failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Report generation failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

}
