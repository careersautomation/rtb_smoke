package ui.smoke.testcases.questionBankASPTool;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.questionBankASPTool_Pages.LoginPage;
import pages.questionBankASPTool_Pages.SideMenuPage;
import verify.SoftAssertions;

public class TC6_LogoutFromQuestionBankASP {


	@Test(enabled = false, priority = 1)
	public void logoutFromApplication() throws Exception {
		try {
			test = reports.createTest("Logging out from questionBank ASP application");
			test.assignCategory("smoke");

			SideMenuPage sideMenuPage = new SideMenuPage();
			SoftAssertions.assertTrue(isElementExisting(driver,SideMenuPage.logout_link, 5), "User has logged in to application", test);
			System.out.println("TC6");
			sideMenuPage.logout(SideMenuPage.logout_link);
			SoftAssertions.assertTrue(isElementExisting(driver,LoginPage.username_input, 5), "User has logged out of application", test);

						
		
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Logout failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Logout failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

}
