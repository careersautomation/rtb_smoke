package ui.smoke.testcases.housingAnalysisTool;

import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.driver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.housingAnalysisTool_Pages.HomePage;
import pages.housingAnalysisTool_Pages.MicroStrategyPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_LoginToHAT extends InitTests{


		public TC1_LoginToHAT(String appName) {
			super(appName);
			// TODO Auto-generated constructor stub
		}
		@Test(enabled = true)
		public void login_hat() throws Exception {
			try {
				System.out.println("test started");
				test = reports.createTest("Logging in to Housing analysis tool");
				test.assignCategory("smoke");

				TC1_LoginToHAT login = new TC1_LoginToHAT("hat");

				System.out.println("Logging in to application");
				//String url,String browserType,String browserVersion,String platform,String executionEnv,ExtentTest test,String nodeUrl
				initWebDriver(BASEURL, "IE", "", "", "local", test, "");
				System.out.println("BaseURL is: " + BASEURL);

				MicroStrategyPage microStrategyPage = new MicroStrategyPage();
				microStrategyPage.loginToApplication(USERNAME);
				
				HomePage homePage = new HomePage();
				SoftAssertions.assertTrue(isElementExisting(driver, HomePage.inProgress_button, 15), "User has logged in to application", test);
				

			} catch (Error e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} catch (Exception e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			}
			

		}
		
		@AfterSuite
		public void killDriver() {
			reports.flush();
			driver.close();
			killBrowserExe(BROWSER_TYPE);
			
		}
	}

