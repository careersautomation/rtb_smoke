package ui.smoke.testcases.housingAnalysisTool;

import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.driver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.housingAnalysisTool_Pages.LogoutPage;
import pages.housingAnalysisTool_Pages.SurveyPage;
import verify.SoftAssertions;

public class TC3_logoutFromHAT {
	@Test(enabled = true, priority = 1)
	public void logoutFromHAT() throws Exception {
		try {
			test = reports.createTest("Logout from HAT");
			test.assignCategory("smoke");

			SurveyPage surveyPage = new SurveyPage();
			SoftAssertions.assertTrue(isElementExisting(driver, SurveyPage.survey_title, 5), "Survey page is openend", test);
			
			surveyPage.logout();
			LogoutPage logoutPage = new LogoutPage();
			SoftAssertions.assertTrue(isElementExisting(driver,LogoutPage.loggedOut_dialog, 5), "User has logged out from application", test);
			
			

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("logging out from application failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("logging out from application failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
	

		}
}
