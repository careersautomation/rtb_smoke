package ui.smoke.testcases.housingAnalysisTool;

import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.housingAnalysisTool_Pages.HomePage;
import pages.housingAnalysisTool_Pages.SurveyPage;
import verify.SoftAssertions;

public class TC2_accessAndModifySurveyContent {

	
	@Test(enabled = true, priority = 1)
	public void navigateToSurveyPage() throws Exception {
		try {
			test = reports.createTest("Navigating to  survey content");
			test.assignCategory("smoke");

			HomePage homePage = new HomePage();
			SoftAssertions.assertTrue(isElementExisting(driver,HomePage.inProgress_button, 5), "User is on the home page", test);
			
			homePage.selectInProgressTab();
			SoftAssertions.assertTrue(isElementExisting(driver,HomePage.inProgress_buttonSelected, 10), "In progress tab selected", test);
			
			homePage.selectSurvey();
			SurveyPage surveyPage = new SurveyPage();
			SoftAssertions.assertTrue(isElementExisting(driver,SurveyPage.survey_title, 5), "Survey page is openend", test);
			
			

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Navigation to survey failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Navigation to survey failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		
		}
		
		@Test(enabled = true, priority = 2)
		public void modifyingSurveyPage() throws Exception {
			try {
				test = reports.createTest("modifying  survey content");
				test.assignCategory("smoke");

				SurveyPage surveyPage = new SurveyPage();
				SoftAssertions.assertTrue(isElementExisting(driver, SurveyPage.survey_title, 5), "Survey page is openend", test);
				
				String oldContent =surveyPage.getSurveyContent(); 
				surveyPage.editContent();
				
				String newContent = surveyPage.getSurveyContent();
				String expectedContent = oldContent+"TEST";
//				String contentArray[] = {newContent, expectedContent};
				
				SoftAssertions.verifyContains(newContent, expectedContent,"" ,test);
				
				SurveyPage surveyPageNew = new SurveyPage();
				surveyPageNew.resetContent(oldContent);
							

			} catch (Error e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("Modification to survey failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} catch (Exception e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("Modification to survey failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			}
			

	}
		
		@Test(enabled = true, priority = 2)
		public void verifyPreview() throws Exception {
			try {
				test = reports.createTest("Verify preview file");
				test.assignCategory("smoke");

				SurveyPage surveyPage = new SurveyPage();
				SoftAssertions.assertTrue(isElementExisting(driver, SurveyPage.survey_title, 5), "Survey page is openend", test);

				String actualTitle = surveyPage.viewPreview();
				String expectedTitle = "HAT Comments/Alerts. MicroStrategy 9";
//				String urlArray[] = {actualURL, expectedURL};
				SoftAssertions.verifyContains(actualTitle, expectedTitle,"", test);
				SoftAssertions.assertTrue(isElementExisting(driver, SurveyPage.survey_title, 5), "Survey page is openend", test);
				
			} catch (Error e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("Comments alerts pdf download failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} catch (Exception e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("Comments alerts pdf download failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			}
			
	}
	
	
}
