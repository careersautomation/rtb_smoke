package ui.smoke.testcases.eipeblue;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.switchToWindow;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.io.IOException;
import java.util.Set;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.epieblue_Pages.BenchmarkCodeInfoPage;
import pages.epieblue_Pages.GroupBenchmarkListPage;
import pages.epieblue_Pages.HeaderPage;
import pages.epieblue_Pages.RunReportListPage;
import verify.SoftAssertions;

//import org.sikuli.script.Pattern;
//import org.sikuli.script.Screen;

public class TC2_GenerateGroupBenchmarkListAndRunReport {

	
	@Test(enabled = true, priority = 1)
	public void goToGroupBechmarkList() throws IOException {
		try {
		test = reports.createTest("Navigating to Group Benchmark list");
		test.assignCategory("smoke");
		
		System.out.println("Navigating to Group Benchmark list");
		
		HeaderPage headerPage = new HeaderPage();
		hoverAndClickOnElement(driver,HeaderPage.group_menuLink, HeaderPage.groupBenchmarkList_subMenu);
		
		GroupBenchmarkListPage gblPage = new GroupBenchmarkListPage();
		SoftAssertions.assertTrue(isElementExisting(driver, GroupBenchmarkListPage.GroupBenchmarkListPageTitle, 5), "Group Benchmark List page displayed", test);
		
		
		gblPage.showAllResults();
		SoftAssertions.assertTrue(isElementExisting(driver,GroupBenchmarkListPage.groupBenchmarkListTable, 5), "Table is displayed", test);
		
		
		
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigating to Group Benchmark list FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigating to Group Benchmark list FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	@Test(enabled = true , priority = 2)
	public void verifyGroupBenchmarkInfo() throws IOException {
		try {
			
			test = reports.createTest("Verify content of group benchmark code");
			test.assignCategory("smoke");
			
			
			System.out.println("Verify content of group benchmark code");
			
			
			GroupBenchmarkListPage gblPage = new GroupBenchmarkListPage();
			String expectedCode = gblPage.getGroupBenchmarkInfoText(GroupBenchmarkListPage.groupBenckmarkCode_text);
			String expectedGroupName = gblPage.getGroupBenchmarkInfoText(GroupBenchmarkListPage.groupName_text);
			String expectedTitle = gblPage.getGroupBenchmarkInfoText(GroupBenchmarkListPage.groupBenchmarkTitle_text);
			
			gblPage.navigateToBenchMarkCodeInformation(GroupBenchmarkListPage.groupBenchmarkTitle_text);
			
			BenchmarkCodeInfoPage benchmarkCodeInfoPage = new BenchmarkCodeInfoPage();
			SoftAssertions.assertTrue(isElementExisting(driver,BenchmarkCodeInfoPage.codingInfo_SideMenu, 5), "Benchmark code information is displayed",test);
			
			SoftAssertions.verifyElementTextContains(BenchmarkCodeInfoPage.groupBenckmarkCode_text, expectedCode,test);
			SoftAssertions.verifyElementTextContains(BenchmarkCodeInfoPage.groupName_text, expectedGroupName,test);
			SoftAssertions.verifyElementTextContains(BenchmarkCodeInfoPage.groupBenchmarkTitle_text, expectedTitle,test);
			
			benchmarkCodeInfoPage.returnToGroupBenchmarkList();
			SoftAssertions.assertTrue(isElementExisting(driver, GroupBenchmarkListPage.groupBenchmarkListTable, 5), "Table is displayed",test);
			
			
		}
		catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Verify content of group benchmark code FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Verify content of group benchmark code FAILED", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	//disabled as it opens .asp pop up which can not be handled by selenium
	// Thus scripting is not finished for this scenario
	@Test(enabled = false, priority = 3)
	public void runReportForGBL() throws IOException {
		try {
		test = reports.createTest("Run report for Group Benchmark list");
		test.assignCategory("smoke");
		
		GroupBenchmarkListPage gblPage = new GroupBenchmarkListPage();
		SoftAssertions.assertTrue(isElementExisting(driver,GroupBenchmarkListPage.groupBenchmarkListTable, 5), "Table is displayed",test);
		
		gblPage.selectRows();
		
		String parentHandle = driver.getWindowHandle();
		System.out.println("Window Handle for parent window is:  "+parentHandle+"------------------------------");
		
		HeaderPage headerPage = new HeaderPage();
//		updated(HeaderPage.report_menuLink, HeaderPage.reportList_subMenu);
//		clickElement(HeaderPage.reportList_subMenu);
//		String filepath = "C:\\Users\\nidhi-joshi\\Desktop\\sikuliImages\\";
//		Screen s = new Screen();
//		Pattern dropdown = new Pattern(filepath + "selectFromDropdown.PNG");
//		
//		s.click(dropdown);
		
		Set<String> windowArr1 = driver.getWindowHandles();
		System.out.println("Windows present are:    "+ windowArr1.size()+"------------------------");
		
		RunReportListPage runReportListPage = new RunReportListPage();
		
		switchToWindow("Mercer eIPE -- webpage dialog");
		SoftAssertions.assertTrue(isElementExisting(driver,RunReportListPage.reportMenu_Dropdown, 5), "Switched to new window and run report screen is present",test);
		
		runReportListPage.selectReportType("Group Benchmark List");
		
		Set<String> windowArr2 = driver.getWindowHandles();
		System.out.println("Windows present are:    "+ windowArr2.size()+"------------------------");
		
		
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}
	
	
}
