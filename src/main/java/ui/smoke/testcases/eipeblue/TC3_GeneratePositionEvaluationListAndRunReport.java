package ui.smoke.testcases.eipeblue;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.io.IOException;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.epieblue_Pages.BenchmarkCodeInfoPage;
import pages.epieblue_Pages.GroupBenchmarkListPage;
import pages.epieblue_Pages.HeaderPage;
import pages.epieblue_Pages.PositionEvaluationInfoPage;
import pages.epieblue_Pages.PositionEvaluationListPage;
import verify.SoftAssertions;

public class TC3_GeneratePositionEvaluationListAndRunReport {

	@Test(enabled = true, priority = 1)
	public void goToPositionEvaluationList() throws IOException {
		try {

			test = reports.createTest("Navigating to position evaluation list");
			test.assignCategory("smoke");

			System.out.println("Navigating to position evaluation list");

			HeaderPage headerPage = new HeaderPage();
			headerPage.navigatingViaMenu(HeaderPage.home_menuLink);
			
			SoftAssertions.assertTrue(isElementExisting(driver,HeaderPage.pageTitle, 10),"Home page is displayed",test);
			
			
			hoverAndClickOnElement(driver,HeaderPage.position_menuLink, HeaderPage.positionEvaluationList_subMenu);

			PositionEvaluationListPage pelPage = new PositionEvaluationListPage();
			SoftAssertions.assertTrue(isElementExisting(driver,PositionEvaluationListPage.positionEvaluationListPageTitle, 5),
					"Position Evaluation List page is displayed",test);

			pelPage.showAllResults();
			SoftAssertions.assertTrue(isElementExisting(driver,PositionEvaluationListPage.poitionEvaluationListTable, 5),
					"Position Evaluation List Table is displayed",test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigating to position evaluation list FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Navigating to position evaluation list FAILED", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

	@Test(enabled = true, priority = 2)
	public void verifyPositionEvaluationForTitle() throws Exception {
		try {
			test = reports.createTest("Verify the content of position evaluation page for corresponding title");
			test.assignCategory("smoke");

			System.out.println("Verify the content of position evaluation page for corresponding title");

			PositionEvaluationListPage pelPage = new PositionEvaluationListPage();
			String expectedTitle = pelPage.getPositionEvaluationTitleText(PositionEvaluationListPage.firstElement);

			pelPage.navigateToPositionEvaluationInformation(PositionEvaluationListPage.firstElement);

			PositionEvaluationInfoPage positionEvaluationeInfoPage = new PositionEvaluationInfoPage();
			SoftAssertions.assertTrue(isElementExisting(driver,PositionEvaluationInfoPage.positionEvaluationResultTitle, 5),
					"Position Evaluation information is displayed",test);

			SoftAssertions.verifyElementTextContains(PositionEvaluationInfoPage.positionEvaluationResultTitle,
					expectedTitle,test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Verify the content of position evaluation page for corresponding title FAILED", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Verify the content of position evaluation page for corresponding title FAILED", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

}
