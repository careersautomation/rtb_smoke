package ui.smoke.testcases.eipeblue;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.epieblue_Pages.HeaderPage;
import pages.epieblue_Pages.LoginPage;
import verify.SoftAssertions;

public class TC4_LogoutFromeEipeBlue {
	
	
	@Test(enabled = true, priority = 1)
	public void logoutFromEipeBlue() throws IOException {
		try {
		test = reports.createTest("Perform logout from the application");
		test.assignCategory("smoke");
		
		System.out.println("Perform logout from the application");
		
		HeaderPage headerPage = new HeaderPage();
		headerPage.logout();
		
		
		LoginPage loginPage = new LoginPage();
		SoftAssertions.assertTrue(isElementExisting(driver,LoginPage.email_Input, 5), "User has logged out from application", test);
		
		
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Logout failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Logout failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}finally{
			
			reports.flush();
			driver.close();
		
		}
	}
	
	
}
