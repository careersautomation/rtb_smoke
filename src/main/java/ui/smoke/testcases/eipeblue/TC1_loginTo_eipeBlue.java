package ui.smoke.testcases.eipeblue;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.epieblue_Pages.HeaderPage;
import pages.epieblue_Pages.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_loginTo_eipeBlue extends InitTests {

	public TC1_loginTo_eipeBlue(String appName) {
		super(appName);
		// TODO Auto-generated constructor stub
	}

	@Test(enabled = true)
	public void login_eIPEBlue() throws Exception {
		try {
			test = reports.createTest("Logging in to eIPE Blue application");
			test.assignCategory("smoke");

			System.out.println("Logging in to application");
			TC1_loginTo_eipeBlue login = new TC1_loginTo_eipeBlue("eIPEBlue");

			System.out.println("Logging in to application");
			// argument list for following method
			//String url,String browserType,String browserVersion,String platform,String executionEnv,ExtentTest test,String nodeUrl
			initWebDriver(BASEURL, "IE", "","","local", test, "");
			System.out.println("BaseURL is: " + BASEURL);

			LoginPage loginPage = new LoginPage();
			loginPage.login(USERNAME, PASSWORD);

			HeaderPage headerPage = new HeaderPage();
			SoftAssertions.verifyElementTextContains(HeaderPage.logOut_Button, "Sign Out", test);

		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}

	@AfterSuite
	public void killDriver() {

		killBrowserExe(BROWSER_TYPE);

	}

}
