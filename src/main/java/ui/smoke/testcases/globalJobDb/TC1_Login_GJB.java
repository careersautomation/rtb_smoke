package ui.smoke.testcases.globalJobDb;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.GlobalJobDb_Pages.Homepage;

import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static  driverfactory.Driver.*;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_Login_GJB extends InitTests {

	

	@Test(enabled = true)
	
	public void login_GlobalJobDb() throws Exception {
		
		try {
			test = reports.createTest("Logging in to Global Job Database");
			test.assignCategory("smoke");
			//deleteCookies();
			
			System.out.println("Opening the application");
			initWebDriver("http://qa12-mgd.mrshmc.com/MGD/UI/Home.aspx","IE","","","local",test,"");
			System.out.println("BaseURL is: " + BASEURL);
			
			//System.out.println((Homepage.Header).getText());
			
			Homepage homepage = new Homepage();
			System.out.println();
			
			waitForElementToDisplay(Homepage.Header);
			SoftAssertions.verifyElementTextContains(Homepage.Header, "What is the Global Job Database (GJD)?",test);
			//SearchJobPage searchJob = new SearchJobPage();
			
			
		}  catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
	}
}
}
