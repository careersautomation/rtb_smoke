package ui.smoke.testcases.globalJobDb;

import org.testng.annotations.Test;

import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;

import static driverfactory.Driver.*;

import pages.GlobalJobDb_Pages.SearchJobPage;
import verify.SoftAssertions;

public class TC2_SearchJob_GJB {

	@Test(enabled = true)
	public  void search_GlobalJobDb() throws Exception {
		
		try {
			test = reports.createTest("Verifying User can Search Job");
			test.assignCategory("smoke");
			
			SearchJobPage searchJob = new SearchJobPage();
			
			searchJob.menuHovering(searchJob.JobHeader,searchJob.JobHeader1);
			
			System.out.println("done");
			
			
			searchJob.selectFromDropdown(searchJob.jobsDropdown, "Career Stream");
			
			searchJob.selectFromDropdown(searchJob.jobsDropdown2, "is");
			
			searchJob.enterInput(searchJob.jobsDropdown3, "Executive");
			
			searchJob.btnClick(SearchJobPage.goButton);
			
			
			/*String text = document.getElementById('UltWebGridSearchResult_rc_0_3').innerText();*/
			
			/*String theTextIWant = ((JavascriptExecutor) driver).executeScript("return arguments[0].innerHTML;",driver.findElement(By.xpath("//span[@itemprop='telephone']")));
			*/
			
			
			//Driver.clickElementUsingJavaScript(searchJob.jobFamilyName);
			Boolean text = Driver.isElementExisting(driver,SearchJobPage.excelData, 10);
			SoftAssertions.assertTrue(text, "Yes the excel has loaded !!",test);
			System.out.println(text);
			
			//SoftAssertions.verifyElementText(searchJob.excelData, "adil_job", "The excel data is present");
			
			searchJob.btnClick(SearchJobPage.exportExcel);
			//As the page is not available the assertions for exporting excel is yet to be coded
			
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Reports did not load", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Excel was not exported", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
	}

}
