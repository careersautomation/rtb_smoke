package ui.smoke.testcases.globalJobDb;

import static driverfactory.Driver.*;

import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static driverfactory.Driver.*;

import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.GlobalJobDb_Pages.ReportsJobPage;
import verify.SoftAssertions;


public class TC3_ReportsJob_GJB {
	
	@Test(enabled = true)
	public void TC3_Reports_GJB () throws Exception{
		
		try {
			
			test = reports.createTest("Verifying User can download Reports");
			test.assignCategory("smoke");
			
			ReportsJobPage reports = new ReportsJobPage();
		
			
			reports.menuHovering(reports.Report_Navbar, reports.JobFamily_Navbar);
			
			reports.btnClick(ReportsJobPage.ShowAll_Btn);
			
			Boolean texts = reports.checkPresenceOfElement(reports.assert_reports, 20);
			System.out.println(texts);
			SoftAssertions.assertTrue(texts, "Yes,The excel is present !!",test); 
			
			reports.btnClick(ReportsJobPage.Excel_Export);
			
			/*Since the report cannot be downloaded in the QA environment assertion of report download is still remaining.*/
			
			reports.menuHovering(reports.Admin, reports.JobFamily_Admin, reports.SearchJobFamily);
			
			
			reports.btnClick(ReportsJobPage.ShowAll_Btn);
			
			Boolean text = reports.checkPresenceOfElement(reports.assert_reports, 20);
			System.out.println(text);
			SoftAssertions.assertTrue(text, "Yes,The excel is present !!",test);
			
			
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Search Job page is not displayed.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Search Job page is not displayed.", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		
		
	}

}
