package ui.smoke.testcases.globalJobDb;

import org.testng.annotations.Test;

import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import pages.GlobalJobDb_Pages.SurveyJobPage;
import verify.SoftAssertions;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import atu.testng.reports.ATUReports;
import static driverfactory.Driver.*;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import static utilities.InitTests.*;

public class TC4_SurveyJob_GJB {
	
	@Test(enabled= true)
	public void surveyExcel_GJB() throws Exception {
		
		try {			
			test = reports.createTest("Verifying Survery page");
			test.assignCategory("smoke");
			
			SurveyJobPage surveyJobPage = new SurveyJobPage();
		
			surveyJobPage.menuHovering(SurveyJobPage.Admin, SurveyJobPage.SurveyLink, SurveyJobPage.SearchSurvey);
			
			surveyJobPage.btnClick(SurveyJobPage.ShowAll_Btn);
			
			Boolean text = surveyJobPage.checkPresenceOfElement(surveyJobPage.excel_Data, 20);
			System.out.println(text);
			SoftAssertions.assertTrue(text, "Yes,The excel is present !!",test);
			
			
			
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Survery Job page is not displayed.", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Survery Job page is not displayed.", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
		finally {
			reports.flush();
			driver.close();
			killBrowserExe(BROWSER_TYPE);
			

		}
	}

}
