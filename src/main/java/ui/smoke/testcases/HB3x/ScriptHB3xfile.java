package ui.smoke.testcases.HB3x;
import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;

import org.openqa.selenium.By;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import com.thoughtworks.selenium.webdriven.commands.IsElementPresent;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;

import pages.HB3x.Script_pages;
import utilities.InitTests;
import verify.SoftAssertions;

public class ScriptHB3xfile    extends InitTests {
	
	public ScriptHB3xfile  (String appName) {
		super(appName);
		
	}
	
	@Test(enabled = true)
	public void login() throws Exception{
		try {
			test = reports.createTest("Logging in to HB3x application");
			test.assignCategory("smoke");
			ScriptHB3xfile login = new ScriptHB3xfile  ("HB3x_");
			initWebDriver(BASEURL,BROWSER_TYPE,"","","local",test,"");
			//initWebDriver(BROWSER_TYPE, BASEURL);
			//driver.get(BASEURL);
			
			Script_pages loginPage = new Script_pages(); 
			/*int size = driver.findElements(By.tagName("iframe")).size();
			System.out.println(size);*/
			//driver.switchTo().frame(1);
			loginPage.Login_Functionality_Click(USERNAME,PASSWORD);	
			verifyElementTextContains(loginPage.Health_Welfare,"Health and Welfare",test);
			loginPage.Health_Welfare_Click();
			verifyElementTextContains(loginPage.My_Personal_Data,"My Personal Data",test);
			loginPage.My_Personal_Data_Click();
			loginPage.My_Personal_DataChange_Click();
			loginPage.My_Beneficiary();
			//loginPage.My_Current_Coverage_Click();
			loginPage.Logout_Click();
			
			
			
			
			
		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.DESKTOP));

		}

}
	@AfterSuite
	public void tearDown()
	{
	reports.flush();
		driver.quit();
	}



}

/*Boolean Verify=Driver.isElementExisting(loginPage.PurchasejobButton, 10);
SoftAssertions.assertTrue(Verify, "HomepagePage Verification");*/

