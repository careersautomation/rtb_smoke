package pages.PayMonitor_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindowWithURL;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.*;

import java.util.Random;

import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;


public class ReportServices_Pages {

	/*@FindBy(xpath= "")
	public static WebElement ;*/

	///html/body/div[2]/div[1]/div/div/p[2]/a[1]

	@FindBy(xpath= "/html/body/div[2]/div[2]/div/img")
	public static WebElement alertLink;

	@FindBy(xpath= "//a[@id= 'tsHoriz__ctl2_PostedReports1__ReportDataGrid_ctl02_ViewPDFLink']")
	public static WebElement view_link;

	@FindBy(id= "tsHoriztd2")
	public static WebElement MyReports_tab;

	@FindBy(id= "WizardBody1_ctl01_chkNotification")
	public static WebElement notificationCheckbox;

	@FindBy(id= "WizardBody1_ctl01_txtEmailAddress")
	public static WebElement emailAddr;

	@FindBy(xpath= "//a[@id='Wizardnavigatorlinks1_ctl10_ctl05']")
	public static WebElement generateReport_link;

	@FindBy(xpath= "//a[@id='btnOK']")
	public static WebElement ok_link;

	@FindBy (xpath= "//a[@id='btnSelectAll']")
	public static WebElement selectAll_link;

	@FindBy(xpath= "//td[@id='tsHoriz__ctl0_PurchasedSurveys1__SurveyDataGrid_ctl01__LocationFilterCell']/select")
	public static WebElement country_dropdown;

	@FindBy(xpath= "//td[@id='tsHoriz__ctl0_PurchasedSurveys1__SurveyDataGrid_ctl01__YearFilterCell']/select")
	public static WebElement year_dropdown;

	@FindBy(xpath= "//a[@id ='tsHoriz__ctl0_PurchasedSurveys1__SurveyDataGrid_ctl02_CustomizeSurveyLink']")
	public static WebElement customize_link;

	@FindBy(xpath= "//a[@id='tsHoriz__ctl0_ReportTemplates1_DataGrid1_ctl02_RunTemplateLink']")
	public static WebElement run_link;

	@FindBy(xpath= "//span[@id='WizardBody1_ctl01_dualJobPositions_MoveAllRightArea']/a")
	public static WebElement btnAll_link;

	@FindBy(xpath= "//a[@id='WizardBody1__WizardNavigation__Next']")
	public static WebElement next_btn;

	@FindBy(id= "WizardBody1_ctl01_txtRptTitle")
	public static WebElement reportTitle;

	@FindBy(xpath= "//a[@id='WizardBody1__WizardNavigation__Finish']")
	public static WebElement submitLink;

	@FindBy(xpath = "//span[@id='MainBanner1__MainBannerLogoutLink_TextArea']")
	public static WebElement exitPM_link;
	
	@FindBy(id="WizardBody1_ctl01_lstQueriesList")
	public static WebElement selectMarketList;

	public void scrollAndClickNextBtn () throws InterruptedException {
		waitForElementToDisplay(next_btn);
		scrollToElement(next_btn);
		clickElement(next_btn);
	}

	public void clickPayMonitorLink() {
		// TODO Auto-generated method stub
		waitForElementToDisplay(exitPM_link);
		clickElement(exitPM_link);
	}

	public void clickViewLink() {

		while(!(view_link.getText().equals( " View"))) {

			Driver.refreshpage();
			clickElement(MyReports_tab);

		}
		clickElement(view_link);
	}

	public void switchWindow(String Oldhandle) throws InterruptedException {

		
		switchToWindowWithURL(" https://qa12-paymonitor.imercer.com/PayMonitor/Pages/SQMHandler.aspx?mode=2&id=1&v=1&sc=EUDEM_BE03&title=Sector%20(Industry)");
		System.out.println("Switched to new window");
		driver.manage().window().maximize();
		waitForElementToDisplay(selectAll_link);
		clickElement(selectAll_link);
		waitForElementToDisplay(ok_link);
//		hoverAndClickOnElement(driver, ok_link, ok_link);
		hoverOverElement(driver, ok_link);
		try {
			ok_link.click();
		}
		catch(NoSuchWindowException e){
			switchToWindow(Oldhandle);
			System.out.println("Switched to original window");
		}
		
		
	}

	public void clickMyReports() {

		clickElement(MyReports_tab);
	}
	public void selectCountryFromDropdown(String option) {

		waitForElementToEnable(country_dropdown);
		selEleByVisbleText(country_dropdown, option);

	}
	public void scrollAndClickSubmit() throws InterruptedException {

		scrollToElement(submitLink);
		clickElement(submitLink);
	}

	public void selectYearFromDropdown(String option) throws InterruptedException {


		waitForElementToEnable(year_dropdown);
		selEleByVisbleText(year_dropdown, option);
		Driver.waitForPageLoad();

	}

	public void enterReportTitle() {
		Random rand = new Random();
		setInput(reportTitle, "Report No "+ rand.nextInt(300));
	}
	public String selectOptionFromDropdown() throws InterruptedException {
		String currWindow = driver.getWindowHandle();
		waitForElementToEnable(selectMarketList);
		selEleByVisbleText(selectMarketList, "Sector (Industry)"); 
		return(currWindow);
	}

	public void enterEmail() {

		emailAddr.clear();
		setInput(emailAddr, "vidya.dandinashivara@mercer.com");
	}

	public void loadAndClickCustomizeLink() throws InterruptedException {

		Thread.sleep(3000);
		clickElement(alertLink);
		clickElement(customize_link);
		Driver.waitForPageLoad();
	}

	public void loadAndClickRunLink() {

		clickElement(run_link);
		Driver.waitForPageLoad();
	}
	public void loadAndClickBtnLink() {

		clickElement(btnAll_link);
	}
	public void loadAndClick(WebElement element) {
		Driver.waitForPageLoad();
		clickElement(element);
	}
	public void pageLoadWait() {
		Driver.waitForPageLoad();
	}

	public void clickGenerateReport() {

		clickElement(generateReport_link);
	}
	public void verifyCheckbox() {

		if(!notificationCheckbox.isSelected()) {
			System.out.println("Checkbox not checked !!");
			notificationCheckbox.click();
		}

	}

	public ReportServices_Pages() {
		PageFactory.initElements(driver, this);
	}
}
