package pages.PayMonitor_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;

public class RegionSelectionPage {

	@FindBy(id = "ctl01_uxRegionsDataList_ctl00_uxRegionHyperlink")
	public static WebElement region_Link;

	public RegionSelectionPage() {
		PageFactory.initElements(driver, this);
	}

	// public void checkAndSelectRegionElement(D) {
	// int count = (driver.findElements(By.id("ctl01_locationLabel"))).size();
	// if (count > 0) {
	// System.out.println("Count of region: " + count);
	// clickElement(region_Link);
	// }
	// }

// waitForElementToDisplay(myAccount_Link);
// }
// else {
// waitForElementToDisplay(myAccount_Link);
// }

	public void clickRegionLink(String region) {
		WebElement ele = driver.findElement(By.xpath("//a[contains(text(),'" + region + "')]"));
		clickElement(ele);
	}
}