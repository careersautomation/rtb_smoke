package pages.PayMonitor_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PayMonitor_Dashboard {
	
	@FindBy(id = "MainBanner1__MainBannerLogoutLink_TextArea")
	public static WebElement exitPayMonitor_Link;
	
	@FindBy (id = "MainBanner1__MainBannerMyPayMonitorLink_TextArea")
	public static WebElement myPayMonitor_Link;

	@FindBy(name = "selFilterByLocation")
	public static WebElement country_dropdown;
	
	@FindBy(name = "selFilterByYear")
	public static WebElement year_dropdown;
	
	public PayMonitor_Dashboard() {
		PageFactory.initElements(driver, this);
	}
	

}
