package pages.PayMonitor_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.navigateBack;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IMercer_HomePage {

	@FindBy(id = "myaccounts")
	public static WebElement myAccountsBtn;
	
	@FindBy(xpath= "//td[@id='tsHoriz__ctl0_PurchasedSurveys1__SurveyDataGrid_ctl01__LocationFilterCell']/select")
	public static WebElement country_dropdown;

	@FindBy(id = "Header1_iMercerHeader_iMercerMiniHeader_hyperlnkSignIn")
	public static WebElement signIn_Link;
	
	@FindBy(id = "Header1_iMercerHeader_iMercerMiniHeader_hyperlnkLogout")
	public static WebElement logout;

	@FindBy(xpath = "//a[@id='myaccounts']")
	public static WebElement account_Link;

	@FindBy(id = "ctl00_MainSectionContent_Email")
	public static WebElement email_input;

	@FindBy(id = "ctl00_MainSectionContent_Password")
	public static WebElement password_input;

	@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
	public static WebElement sign_in_Button;

	@FindBy(xpath = "//input[@id='ctl00_MainSectionContent_Credentials_0']")
	public static WebElement radio;

	@FindBy(xpath = "//button[@id='ctl00_MainSectionContent_ButtonSubmit']")
	public static WebElement sendCode_btn;
	// button[@id='ctl00_MainSectionContent_ButtonSubmit']

	@FindBy(xpath = "//input[@id='username']")
	public static WebElement outlook_username;

	@FindBy(xpath = "//input[@id='password']")
	public static WebElement outlook_password;

	@FindBy(xpath = "//div[@id='lgnDiv']/div[9]/div/span")
	public static WebElement outlook_signIn;

	@FindBy(xpath="//a[@id='btnCookieOptIn']")
	public static WebElement cookieBtn;
	
	@FindBy(id = "ctl03_dlMyToolLinks_ctl00_myToolLink")
	public static WebElement Paymonitor_link;

	@FindBy(xpath = "//span[@id='MainBanner1__MainBannerLogoutLink_TextArea']")
	public static WebElement exitPM_link;
	
	@FindBy(id = "Header1_iMercerHeader_iMercerMiniHeader_hyperlnkSignIn")
	public static WebElement signinBtn;

	public void login_hover() {
		waitForElementToDisplay(myAccountsBtn);
		hoverOverElement(driver,myAccountsBtn);
		clickElementUsingJavaScript(driver,signinBtn);
	}

	public void login_imercer(String username, String password) {
		
		
			setInput(email_input, username);
			setInput(password_input, password);
			clickElement(sign_in_Button);
		
	}
	
	public void login_imercer_ReferenceError(String username, String password) {
		
		if (driver.getPageSource().contains("Contact Us for Help")) {
			driver.navigate().back();
			setInput(email_input, username);
			setInput(password_input, password);
			clickElement(sign_in_Button);
		}
		else {
			System.out.println("Reference Error Not Found");
		}
	}

	public void clickCookieBtn() {
		// TODO Auto-generated method stub
		waitForElementToDisplay(cookieBtn);
		clickElement(cookieBtn);
	}
	
	public void clickPayMonitorLink() {
		// TODO Auto-generated method stub
		waitForElementToDisplay(Paymonitor_link);
		clickElement(Paymonitor_link);
	}

	public IMercer_HomePage() {
		PageFactory.initElements(driver, this);
	}

	public void goBack() {
		navigateBack(driver);
	}

	public void loginPayMonitor2(String password) {
		waitForElementToEnable(password_input);
		setInput(password_input, password);
		clickElement(sign_in_Button);
	}

//	public void newTab_outlook(String username, String password) throws InterruptedException {
//		
//		
//		Thread.sleep(5000);
//		((JavascriptExecutor)driver).executeScript("window.open()");
//		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
//		System.out.println("Nod of windows : "+tabs.size());
//		Thread.sleep(5000);
//		driver.switchTo().window(tabs.get(1));
//		Thread.sleep(5000);
//		webdriver.manage().window().maximize();
//		driver = new EventFiringWebDriver(webdriver);
//		event = new EventListner();
//		driver.register(event);
//		driver.manage().timeouts().setScriptTimeout(defaultTimeOut, TimeUnit.SECONDS);
//		waitForPageLoad();
//		driver.get("https://apac2mail.mmc.com/owa/");
//		
//		
//		//Thread.sleep(5000);
//		//Driver.waitForPageLoad();
//		waitForElementToDisplay(outlook_username);
//		setInput(outlook_username, username);
//		setInput(outlook_password, password);
//		click(outlook_signIn);
//		}
//		
//
//	public void login_outlook(String username, String password) {
//
//		
//	}
}
