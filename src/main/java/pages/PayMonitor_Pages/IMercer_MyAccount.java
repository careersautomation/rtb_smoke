package pages.PayMonitor_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IMercer_MyAccount {
	
	@FindBy(className = "welcome hidden-xs")
	public WebElement welcome_message;
	
	@FindBy(id="ctl03_dlMyToolLinks_ctl00_myToolLink")
	public WebElement payMonitor_Link;
	
	public IMercer_MyAccount() {
		PageFactory.initElements(driver, this);
	}

}
