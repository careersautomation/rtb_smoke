package pages.housingAnalysisTool_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

public class MicroStrategyPage {
	
	@FindBy(xpath = "//div[@class = 'mstrProjectItem']/a[@href = 'Main.aspx?evt=3010&src=Main.aspx.3010&Port=0&Project=MercerMobility_HAT_1.2&Server=USDFW12AS32&loginReq=true']")
	public static WebElement hat_link;
	
	@FindBy(id = "Uid")
	public static WebElement userID_input;
	
	@FindBy(xpath = "//input[@value = 'Login']")
	public static WebElement login_button;
	
	@FindBy(xpath = "//div[@class = 'mstrLargeIconViewItemName']/a[@href = 'Main.aspx?evt=2001&src=Main.aspx.2001&systemFolder=7']")
	public static WebElement sharedFolder_link;
	
	@FindBy(xpath = "//a[@title = 'HAT Survey Management']")
	public static WebElement hat_option;
	
	
	public MicroStrategyPage() {
		PageFactory.initElements(driver, this);
	}

	
	public void loginToApplication(String id) {
		clickElement(hat_link);
		setInput(userID_input, id);
		clickElement(login_button);
		clickElement(sharedFolder_link);
		clickElement(hat_option);
	}
}
