package pages.housingAnalysisTool_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindow;
import static driverfactory.Driver.switchToWindowWithURL;
import static driverfactory.Driver.waitForPageLoad;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SurveyPage {

	@FindBy(xpath = "//div[contains(text(), 'Aveiro - October 2014')]")
	public static WebElement survey_title;

	@FindBy(id = "mstr80")
	public static WebElement surveyTextArea;

	@FindBy(xpath = "//input[@value = 'Save']")
	public static WebElement save_button;

	@FindBy(xpath = "//div[contains(text(),'Comments/Alerts')]")
	public static WebElement commentsAlert_link;

	@FindBy(xpath = "//div[contains(text(),'Preview')]")
	public static WebElement preview_link;

	@FindBy(xpath = "//div[contains(text(),'Sign Out')]")
	public static WebElement logout_link;

	@FindBy(xpath = "//div[contains(text(),'Welcome')]")
	public static WebElement welcome_link;

	public SurveyPage() {
		PageFactory.initElements(driver, this);
	}

	public void editContent() throws AWTException {
		// TODO Auto-generated method stub
		surveyTextArea.sendKeys("TEST");
		System.out.println("Page is scrolling1");
		clickElement(welcome_link);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		System.out.println("Page is scrolling2");
		clickElement(save_button);
		delay(5000);
		waitForPageLoad();

	}

	public String getSurveyContent() {

		return (getElementText(surveyTextArea));
	}

	public void resetContent(String oldContent) throws AWTException {
		
//		WebElement surveyTextAreaNew = driver.findElement(By.id("mstr80"));
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_UP);
			setInput(surveyTextArea, oldContent);
			System.out.println("Page is scrolling3");
			clickElement(welcome_link);
			robot.keyPress(KeyEvent.VK_PAGE_DOWN);
			 System.out.println("Page is scrolling4");
			clickElement(save_button);
			delay(5000);
			waitForPageLoad();

		
	}

	public String viewPreview() {

		clickElement(commentsAlert_link);
		clickElement(preview_link);
		String oldWindow = driver.getTitle();
		delay(5000);
		switchToWindow("Export.MicroStrategy");
		String newTabTitle = driver.getTitle();
		switchToWindow(oldWindow);

		return (newTabTitle);
	}

	public void logout() {
		clickElement(logout_link);
	}

}
