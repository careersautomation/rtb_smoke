package pages.housingAnalysisTool_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogoutPage {

	
	@FindBy(xpath = "//span[contains(text(),'Logged out')]")
	public static WebElement loggedOut_dialog;
	
	
	public LogoutPage() {
		PageFactory.initElements(driver, this);
	}
}
