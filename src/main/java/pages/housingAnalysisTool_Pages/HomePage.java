package pages.housingAnalysisTool_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;

public class HomePage {
	
	@FindBy(xpath = "//input[@value = 'In-Progress']")
	public static WebElement inProgress_button;
	
	@FindBy(xpath = "//input[@value = 'In-Progress'][contains(@class , 'selected')]")
	public static WebElement inProgress_buttonSelected;
	
	@FindBy(xpath = "//span[contains(text(), 'Aveiro - October 2014')]")
	public static WebElement survey_link;
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectInProgressTab() {
		clickElement(inProgress_button);
	}

	public void selectSurvey() {
		// TODO Auto-generated method stub
		clickElement(survey_link);
		
	}
	
}
