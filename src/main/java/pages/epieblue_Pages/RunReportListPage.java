package pages.epieblue_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.selEleByVisbleText;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RunReportListPage {

	@FindBy (id="ddlReportMenuItems")
	public static WebElement reportMenu_Dropdown;
	
	@FindBy (id = "rbSelectedRows")
	public static WebElement selectedRows_radiobutton;

	@FindBy (id = "rbAllrows")
	public static WebElement allRows_radiobutton;
	
	@FindBy (id = "rbExcel")
	public static WebElement excel_radiobutton;
	
	@FindBy (id = "rbPreview")
	public static WebElement preview_radiobutton;

	@FindBy (id = "btnOK")
	public static WebElement ok_button;
	
	@FindBy (id = "btnCancel")
	public static WebElement cancel_button;

	
	public RunReportListPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectReportType (String value) {
		selEleByVisbleText(reportMenu_Dropdown, value);
		
	}
}
