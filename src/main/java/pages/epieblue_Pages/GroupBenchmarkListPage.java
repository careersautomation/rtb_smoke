package pages.epieblue_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GroupBenchmarkListPage {


	@FindBy (xpath = "//span[contains(text(), 'Group Benchmark List')]")
	public static WebElement GroupBenchmarkListPageTitle;

	@FindBy (xpath = "//input[@value='Show All']")
	public static WebElement showAll_Button;
	
	@FindBy (id = "ctl00_Content_gvBenchmarkList")
	public static WebElement groupBenchmarkListTable;
	
	@FindBy (xpath = "//table[@id='ctl00_Content_gvBenchmarkList']//tr[2]/td[2]")
	public static WebElement groupName_text;
	
	@FindBy (xpath = "//table[@id='ctl00_Content_gvBenchmarkList']//tr[2]/td[4]")
	public static WebElement groupBenckmarkCode_text;
	
	@FindBy (xpath = "//table[@id='ctl00_Content_gvBenchmarkList']//tr[2]/td[5]/a")
	public static WebElement groupBenchmarkTitle_text;
	
	@FindBy (xpath = "//table[@id = 'ctl00_Content_gvBenchmarkList']//tr/td[1]")
	public static WebElement firstCheckbox;
	
	@FindBy (xpath = "//table[@id = 'ctl00_Content_gvBenchmarkList']//tr/td[3]")
	public static WebElement secondCheckbox;
	
	public GroupBenchmarkListPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void showAllResults() {
		clickElement(showAll_Button);
	}
	
	public String getGroupBenchmarkInfoText(WebElement e){
		return(getElementText(e));
		
	}
	
	public void navigateToBenchMarkCodeInformation(WebElement e) {
		clickElement(e);
		BenchmarkCodeInfoPage benchmarkcodePage = new BenchmarkCodeInfoPage();
		waitForElementToDisplay(BenchmarkCodeInfoPage.codingInfo_SideMenu);
	}
	
	public void selectRows() {
		clickElement(firstCheckbox);
		clickElement(secondCheckbox);
	}
}
