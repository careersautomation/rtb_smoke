package pages.epieblue_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PositionEvaluationInfoPage {

	@FindBy(id="ctl00_Content_hdrActualTitleValue")
	public static WebElement positionEvaluationResultTitle;
	
	
	
	
	public PositionEvaluationInfoPage() {
		PageFactory.initElements(driver, this);
	}
}
