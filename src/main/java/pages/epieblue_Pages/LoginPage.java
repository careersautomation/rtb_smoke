package pages.epieblue_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {


	@FindBy (className = "topicHeader1")
	public static WebElement welcomeMessage;

	@FindBy (id = "ctl00_Content_Login1_UserName")
	public static WebElement email_Input;
	
	@FindBy (id = "ctl00_Content_Login1_Password")
	public static WebElement password_Input;
	
	@FindBy (id = "ctl00_Content_Login1_LoginButton")
	public static WebElement login_Button;

	@FindBy(xpath = "//a[@id='overridelink']")
	public static WebElement accpetCertificate;
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void login(String username, String password) {
		
		int count =(driver.findElements(By.xpath("//a[@id='overridelink']"))).size();
		if(count>0) {
			
			clickElement(accpetCertificate);
		}
		
		setInput(email_Input, username);
		setInput(password_Input, password);
		clickElement(login_Button);
	}

}
