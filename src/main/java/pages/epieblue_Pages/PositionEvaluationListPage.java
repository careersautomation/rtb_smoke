package pages.epieblue_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementText;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PositionEvaluationListPage {
	@FindBy (xpath="//span[@class='topicHeader'][contains(text(), 'Position Evaluation List')]")
	public static WebElement positionEvaluationListPageTitle;

	@FindBy (xpath="//input[@value='Show All']")
	public static WebElement showAll_Button;

	@FindBy (id = "ctl00_Content_gvPositionEvaluationList")
	public static WebElement poitionEvaluationListTable;
	
	@FindBy( id= "ctl00_Content_gvPositionEvaluationList_ctl02_lnkActualTitle")
	public static WebElement firstElement;
	
	public PositionEvaluationListPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	
	public void showAllResults() {
		clickElement(showAll_Button);
	}
	
	public String getPositionEvaluationTitleText(WebElement e){
		return(getElementText(e));
		
	}
	
	public void navigateToPositionEvaluationInformation(WebElement e) {
		clickElement(e);
	}
}
