package pages.epieblue_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderPage {


	@FindBy (id = "ctl00__MainBanner_MHRCTalentLogo")
	public static WebElement talentLogo;
	
	@FindBy (id="ctl00__MainBanner_LoginStatus2")
	public static WebElement logOut_Button;
	
	@FindBy (xpath="//li/span[@class='AspNet-Menu-NonLink'][contains(text(), 'Group')]")
	public static WebElement group_menuLink;
	
	@FindBy (xpath="//li/span[@class='AspNet-Menu-NonLink'][contains(text(), 'Report')]")
	public static WebElement report_menuLink;
	
	@FindBy (xpath = "//li[@class = 'AspNet-Menu-Leaf']/a[contains(text(),'Group Benchmark List')]")
	public static WebElement groupBenchmarkList_subMenu;
	
	@FindBy (xpath = "//span[contains(text(),'Report List')]")
	public static WebElement reportList_subMenu;
	
	@FindBy (xpath="//li[@class='AspNet-Menu-WithChildren']/a[@class='AspNet-Menu-Link'][contains(text(), 'Home')]")
	public static WebElement home_menuLink;

	@FindBy(id="ctl00_Title_lblTopic")
	public static WebElement pageTitle; 
	
	@FindBy (xpath="//li/span[@class='AspNet-Menu-NonLink'][contains(text(), 'Position')]")
	public static WebElement position_menuLink;
	
	@FindBy (xpath = "//li[@class = 'AspNet-Menu-Leaf']/a[contains(text(),'Position Evaluation List')]")
	public static WebElement positionEvaluationList_subMenu;
	
	public HeaderPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void navigatingViaMenu(WebElement headerMenu, WebElement mainMenu) {
		clickElement(headerMenu);
		clickElement(mainMenu);

	}
	public void navigatingViaMenu(WebElement headerMenu) {
		clickElement(headerMenu);

	}
	
	public void logout() {
		clickElement(logOut_Button);
	}
}
