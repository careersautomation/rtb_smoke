package pages.epieblue_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BenchmarkCodeInfoPage {

	@FindBy(id = "ctl00_Content_Wizard1_SideBarContainer_SideBarList_ctl00_SideBarButton")
	public static WebElement codingInfo_SideMenu;
	
	@FindBy(id = "ctl00_Content_Wizard1_txtJobCode")
	public static WebElement groupBenckmarkCode_text;
	
	@FindBy(id = "ctl00_Content_Wizard1_txtGroupName")
	public static WebElement groupName_text;
	
	@FindBy(id = "ctl00_Content_Wizard1_txtJobTitle")
	public static WebElement groupBenchmarkTitle_text;
	
	
	@FindBy(id = "ctl00_Content_Wizard1_StartNavigationTemplateContainerID_StartReturnButton")
	public static WebElement return_Button;
	
	
	
	public BenchmarkCodeInfoPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void returnToGroupBenchmarkList () {
		clickElement(return_Button);
	}
	
}
