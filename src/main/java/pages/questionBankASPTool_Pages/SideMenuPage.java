package pages.questionBankASPTool_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SideMenuPage {


	@FindBy (xpath = "//a[@href = 'main.asp'][contains(text(),'Home')]")
	public static WebElement home_link;
	
	@FindBy (xpath = "//a[@href = 'initselectCompany.asp'][contains(text(),'Select Companies')]")
	public static WebElement selectCompanies_link;
	
	@FindBy (xpath = "//a[@href = 'selectBenefits.asp'][contains(text(),'Select Benefits')]")
	public static WebElement selectBenefits_link;
	
	@FindBy (xpath = "//a[@href = 'View_Edit_Data.asp'][contains(text(),'View Data')]")
	public static WebElement veiwData_link;
	
	@FindBy (xpath = "//a[@href = 'reportSelectionMain.asp?selVal=SFSA'][contains(text(),'Reports')]")
	public static WebElement reports_link;
	
	@FindBy (xpath = "//a[@href = 'logout.asp'][contains(text(),'Logout')]")
	public static WebElement logout_link;
	
	public SideMenuPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void navigateViaSideMenu(WebElement e) {
		clickElement(e);
		waitForPageLoad();
		
	}

	public void logout(WebElement e) {
		clickElement(e);
		waitForPageLoad();
		
	}
}
