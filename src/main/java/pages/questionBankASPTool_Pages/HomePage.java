package pages.questionBankASPTool_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	@FindBy (xpath = "//td[@class = 'question'][contains(text(), 'Welcome to the Report Facility section of the Employee Benefits Survey')]")
	public static WebElement welcome_message;
	

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
}
