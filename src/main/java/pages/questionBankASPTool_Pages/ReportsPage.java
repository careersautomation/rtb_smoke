package pages.questionBankASPTool_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.clickElement;

public class ReportsPage {
	
	@FindBy(xpath = "//td/font[contains(text(),'Selection for Reports :')]")
	public static WebElement reportsPageHeader;

	@FindBy(id = "cmbSelect")
	public static WebElement  selectReport_dropdown;
	
	@FindBy(id = "cmbMyCompany")
	public static WebElement  selectCompany_dropdown;
	
	@FindBy(id = "cmbQuestion")
	public static WebElement  selectQuestionnaire_dropdown;
	
	@FindBy(xpath = "//input[@value = 'Get Report']")
	public static WebElement  getReport_button;
	
	public ReportsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectFromDropdown(WebElement e, String optionToSelect) {
		selEleByVisbleText(e, optionToSelect);
		waitForPageLoad();
		
	}

	public void getReports() {
		 clickElement(getReport_button);
		
	}
}

