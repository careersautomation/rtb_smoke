package pages.questionBankASPTool_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.verifyCheckBox;
import static driverfactory.Driver.wait;
import static driverfactory.Driver.waitForPageLoad;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BenefitsPage {

	@FindBy(xpath = "//select[@name = 'questionaire']")
	public static WebElement Questionnaire_dropdown;

	@FindBy(xpath = "//select[@name = 'countryName']")
	public static WebElement country_dropdown;

	@FindBy(xpath = "//input[@value = 'Select']")
	public static WebElement select_button;

	@FindBy(xpath = "//input[@value = 'checkAll']")
	public static WebElement checkAll_button;

	@FindBy(xpath = "//input[@value = 'UncheckAll']")
	public static WebElement unCheckAll_button;
	
	@FindBy (xpath = "//input[@type = 'checkbox']")
	public static List <WebElement> selectBenefits_checkbox;

	public static String checkbox_loc = "(//input[@type = 'checkbox'])[<<index>>]";

	public static String benefitsName_loc = "(//td//input[@type = 'checkbox']//parent::td)[<<index>>]//following-sibling::td[2]";

	public BenefitsPage() {
		PageFactory.initElements(driver, this);
	}

	public void selectFromDropdown(WebElement e, String optionToSelect) {
		selEleByVisbleText(e, optionToSelect);
		waitForPageLoad();
		
	}
	
	public void checkAll() {
		clickElement(checkAll_button);	
		waitForPageLoad();
		System.out.println("Page loading completed");		
		System.out.println("Total rows present on UI are    "+selectBenefits_checkbox.size());
		
	}
	
	public void uncheckAll() {
		
		clickElement(unCheckAll_button);
		waitForPageLoad();
		System.out.println("Page loading completed");
	}

	public List<String> selectCheckbox(int count) {
		int i = 1;
		WebElement ele_checkbox;
		WebElement ele_benefitName;
		List <String> benefitList = new ArrayList <String>();
		for (i = 1; i<count; i++) {
			
			ele_checkbox = fetchLocators(i, checkbox_loc);
			ele_benefitName = fetchLocators(i, benefitsName_loc);
			if(verifyCheckBox(ele_checkbox)) {
				System.out.println("Element already Selected "+i);
				continue;
			}
			else {
				clickElement(ele_checkbox);
				wait.until(ExpectedConditions.elementSelectionStateToBe(ele_checkbox, true));
				System.out.println("Element Selected "+i);
			}
			
			benefitList.add(ele_benefitName.getText());
		}
		
		return (benefitList);
	}
	
	public WebElement fetchLocators(int i, String loc_val) {
		String loc;
		WebElement webEle_loc;
		
		loc = loc_val.replace("<<index>>", ""+i+"");
		webEle_loc= driver.findElement(By.xpath(loc));
		
		return (webEle_loc);
	}

	public void clickSelect() {
		clickElement(select_button);
		
	}


}
