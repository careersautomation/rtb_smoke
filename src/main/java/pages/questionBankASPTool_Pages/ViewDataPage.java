package pages.questionBankASPTool_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.getElementText;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.waitForPageLoad;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;

public class ViewDataPage {

	@FindBy (xpath = "//td[@class = 'question']/font")
	public static WebElement viewDataPage_header;
	
	@FindBy(xpath = "//select[@name = 'cboBenefit']")
	public static WebElement benefits_dropdown;
	
	@FindBy(xpath = "//select[@name = 'cboBenefit']/option")
	public static List <WebElement> benefits_dropdownOption;

	@FindBy(xpath = "//select[@name = 'cmbMyCompany']")
	public static WebElement company_dropdown;
	
	@FindBy(xpath = "//select[@name = 'cmbMyCompany']/option")
	public static List <WebElement> company_dropdownOption;
	
	@FindBy(xpath = "//select[@name = 'cmbQuestion']")
	public static WebElement questionnaireVersion_dropdown;
	
	@FindBy(xpath = "//input[@id = 'optOrderBenefit'][@type = 'radio']")
	public static WebElement benefitWise_radiobutton;
	
	@FindBy(xpath = "//input [@id = 'optOrderQuestion'][@type = 'radio']")
	public static WebElement questionWise_radiobutton;
	
	@FindBy(xpath = "//input [@id = 'optViewData'][@type = 'radio']")
	public static WebElement submissionData_radiobutton;
	
	@FindBy(xpath = "//input [@id = 'optViewPlan'][@type = 'radio']")
	public static WebElement planSelection_radiobutton;
	
	@FindBy (xpath = "//input[@name='cmdRedrawScreen']")
	public static WebElement retrieveData_button;
	
	@FindBy(xpath = "//table[@id = 'Benefitwise_Data']//b[contains(text(), 'COMPANY')]//parent::td")
	public static Set <WebElement> companyName_text;
	
	@FindBy(id = "Benefitwise_Data")
	public static WebElement viewDataTable;
	
	public ViewDataPage() {
		PageFactory.initElements(driver, this);
	}

	public void selectFromDropdown(WebElement e, String optionToSelect) {
		selEleByVisbleText(e, optionToSelect);
		waitForPageLoad();
		
	}

	public void selectRadioButton( WebElement e) {
		 e.click();
		
	}

	public void retrievedata(WebElement e) {
		 clickElement(e);
		
	}

	public String getCompanyName(WebElement e) {
		
		 return(getElementText(e).trim());
		
	}
}
