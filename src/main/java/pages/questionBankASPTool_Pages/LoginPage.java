package pages.questionBankASPTool_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;



public class LoginPage {
	
	@FindBy (xpath = "//input[@name = 'txtLoginID']")
	public static WebElement username_input;
	
	@FindBy (xpath = "//input[@name = 'txtPassword']")
	public static WebElement password_input;
	
	@FindBy (xpath = "//input[@type = 'submit'][@value = 'Login']")
	public static WebElement login_button;
	

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	
	public void qb_login(String username, String password) {
		
		setInput(username_input, username);
		setInput(password_input, password);
		clickElement(login_button);
	}
}
