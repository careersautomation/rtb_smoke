package pages.questionBankASPTool_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.verifyCheckBox;
import static driverfactory.Driver.wait;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.selEleByVisbleText;

import java.util.ArrayList;
import java.util.List;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SelectCompaniesPage {

	@FindBy (xpath = "//td[@class = 'question'][contains(text(), 'Company selection for reports / data export')]")
	public static WebElement welcome_message;
	
	@FindBy (id = "btn_select")
	public static WebElement select_button;
	
	@FindBy (xpath = "//input[@name = 'btn_CheckAll']")
	public static WebElement checkAll_button;
	
	@FindBy (xpath = "//input[@value = 'UncheckAll']")
	public static WebElement uncheckAll_button;
	
	@FindBy (xpath = "//input[@type = 'checkbox']")
	public static List <WebElement> selectCompany_checkbox;
	
	@FindBy (id = "cmbSurveyCycle")
	public static WebElement surveyCycleName_dropdown;
	
	@FindBy (id = "cmbCountry")
	public static WebElement coutryName_dropdown;
	
//	@FindBy (id = "cmbSurveyCycle")
//	public static WebElement surveyCycleName_dropdown;
	
	public static String checkbox_loc = "(//input[@type = 'checkbox'])[<<index>>]";
	
	public static String companyName_loc = "(//td//input[@type = 'checkbox']//parent::td)[<<index>>]//following-sibling::td[1]";
	
	public SelectCompaniesPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void checkAll() {
		clickElement(checkAll_button);	
		waitForPageLoad();
		System.out.println("Page loading completed");
//		for(WebElement e : selectCompany_checkbox) {
//		wait.until(ExpectedConditions.elementToBeSelected(e));
//		val_verifyCheckBox = verifyCheckBox(selectCompany_checkbox);
//		System.out.println("Element is selected and  function is checkAll   "+verifyCheckBox(e));
//		}
		
		System.out.println("Total rows present on UI are    "+selectCompany_checkbox.size());
		
	}
	
	public void uncheckAll() {
		
		clickElement(uncheckAll_button);
		waitForPageLoad();
		System.out.println("Page loading completed");
//		for(WebElement e : selectCompany_checkbox) {
//			wait.until(ExpectedConditions.elementSelectionStateToBe(e, false));
//			System.out.println("Element is not selected and  function is uncheckcheckAll");
//			}
	}
	
	public void selectFromDropdown(WebElement e, String optionToSelect) {
		selEleByVisbleText(e, optionToSelect);
		waitForPageLoad();
	}
	
	public List<String> selectCheckbox(int count) {
		int i = 1;
		WebElement ele_checkbox;
		WebElement ele_companyName;
		List <String> companyList = new ArrayList <String>();
		String companyName;
		for (i = 1; i<count; i++) {
			
			ele_checkbox = fetchLocators(i, checkbox_loc);
			ele_companyName = fetchLocators(i, companyName_loc);
			if(verifyCheckBox(ele_checkbox)) {
				System.out.println("Element already Selected "+i);
				continue;
			}
			else {
				clickElement(ele_checkbox);
				wait.until(ExpectedConditions.elementSelectionStateToBe(ele_checkbox, true));
				System.out.println("Element Selected "+i);
			}
			companyName = ele_companyName.getText();
			System.out.println("Company Name is "+ companyName);
			
			companyList.add(companyName);
		}
		
		return (companyList);
		
	}
	
	public void clickSelect () {
		clickElement(select_button);
		waitForPageLoad();
	}
	
	public WebElement fetchLocators(int i, String loc_val) {
		String loc;
		WebElement webEle_loc;
		
		loc = loc_val.replace("<<index>>", ""+i+"");
		webEle_loc= driver.findElement(By.xpath(loc));
		
		return (webEle_loc);
	}

	
}
