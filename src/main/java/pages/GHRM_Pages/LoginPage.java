package pages.GHRM_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.InitTests;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForPageLoad;

public class LoginPage extends InitTests {
	
	@FindBy(xpath = "//a[contains(@href,'Mobilize+Housing') and contains(@href,'Production')]")
	public static WebElement MBHousing_Btn;
	
	@FindBy(css = ".mstrTitle")
	public static WebElement titleEle;

	@FindBy(id = "ctl00_MainSectionContent_ButtonSignin")
	public static WebElement signInBtn;

	@FindBy(id = "ctl00_MainSectionContent_Email")
	public static WebElement emailTxt;
	
	@FindBy(id = "ctl00_MainSectionContent_Password")
	public static WebElement passwordTxt;

	@FindBy(id = "3001")
	public static WebElement cancelBtn;

	public LoginPage() {
		super("GHMRV");
		PageFactory.initElements(driver, this);
	}

	public void loginToApp() throws InterruptedException {
		setInput(emailTxt, USERNAME);
		setInput(passwordTxt, PASSWORD);
		clickElementUsingJavaScript(driver,signInBtn);
		Thread.sleep(10000);
	}
	
	public void clickMobilityHousingBtn() {
		clickElementUsingJavaScript(driver,MBHousing_Btn);
		waitForPageLoad();
	}
}
