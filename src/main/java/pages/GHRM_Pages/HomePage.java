package pages.GHRM_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.InitTests;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.waitForElementToDisplay;

public class HomePage {
	
	@FindBy(id = "myaccounts")
	public static WebElement myAccountsBtn;
	
	@FindBy(xpath = "//a[contains(@id,'uxRegionHyperlink') and text()='Asia']")
	public static WebElement asiaLnk;
	
	@FindBy(id = "Header1_iMercerHeader_iMercerMiniHeader_hyperlnkSignIn")
	public static WebElement signinBtn;
	
	@FindBy(id = "welcomeWithName")
	public static WebElement welWithNameElm;
	
	@FindBy(css = "a[id*='myToolMBM']")
	public static WebElement MBMLnk;
	
	@FindBy(xpath= "//span[text()='Asia']")
	public static WebElement asiaElm;
	
	@FindBy(xpath = "//li[contains(text(),'Global HRMonitor')]")
	public static WebElement globalHrMonitorLink;
	
	public HomePage () {
		PageFactory.initElements(driver, this);
	}
	
	public void clickSigninBtn() {
		waitForElementToDisplay(myAccountsBtn);
		hoverOverElement(driver,myAccountsBtn);
		clickElementUsingJavaScript(driver,signinBtn);
	}

	public void clickMercerBenifitsLnk() {
		clickElementUsingJavaScript(driver,MBMLnk);
	}
	
	public void clickAsiaLink() {
		waitForElementToDisplay(asiaLnk);
		clickElementUsingJavaScript(driver,asiaLnk);
	}
	
	public void clickGlobalHRMonitorLink() throws InterruptedException {
		waitForElementToDisplay(myAccountsBtn);
		hoverOverElement(driver,myAccountsBtn);
		clickElementUsingJavaScript(driver,globalHrMonitorLink);
		Thread.sleep(10000);
	}
}
