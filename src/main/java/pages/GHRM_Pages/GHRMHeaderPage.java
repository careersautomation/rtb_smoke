package pages.GHRM_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;

public class GHRMHeaderPage {

	@FindBy(xpath = "//a[text()='Calculators']")
	public static WebElement calculatorsLnk;
	
	@FindBy(xpath = "//a[text()='Contacts']")
	public static WebElement contactsLnk;
	
	@FindBy(xpath = "//a[text()='Reports']")
	public static WebElement reportsLnk;
	
	public GHRMHeaderPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void clickContactsLink() {
		clickElementUsingJavaScript(driver,contactsLnk);
	}
	
	public void clickCalculatorsLink() {
		clickElementUsingJavaScript(driver,calculatorsLnk);
	}
	
	public void clickReportsLink() throws InterruptedException {
		clickElementUsingJavaScript(driver,reportsLnk);
		Thread.sleep(5000);
	}
		
}
