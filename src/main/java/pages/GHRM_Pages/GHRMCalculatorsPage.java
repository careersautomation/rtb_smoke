package pages.GHRM_Pages;

import org.jfree.date.AnnualDateRule;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindow;
import java.util.List;

public class GHRMCalculatorsPage {

	@FindBy(className = "SubPageHeaderText")
	public static WebElement calPageTitleElm;

	@FindBy(name = "baseCountry")
	public static WebElement baseCountryLst;

	@FindBy(name = "baseCity")
	public static WebElement baseCityLst;

	@FindBy(name = "hostCountry")
	public static WebElement hostCountryLst;

	@FindBy(name = "hostCity")
	public static WebElement hostCityLst;

	@FindBy(name = "Submit")
	public static WebElement calculateBtn;

	@FindBy(xpath = "//form[@name='wmmForm']/table")
	public static WebElement COLResultTbl;

	@FindBy(name = "C")
	public static WebElement SIHHCountryLst;

	@FindBy(name = "GS")
	public static WebElement SIHHAnnualSalTxt;

	@FindBy(name = "income")
	public static WebElement COLAAnnualSalTxt;

	@FindBy(id = "Radio1")
	public static WebElement viewexchangeRateRdo;

	@FindBy(id = "selBaseCountryISI")
	public static WebElement ISTBaseCountryLst;

	@FindBy(id = "btnCreateOwnTable")
	public static WebElement EACCCreateOwnTableBtn;

	@FindBy(name = "qolTravel")
	public static WebElement QOLTravelChk;

	@FindBy(name = "qolLang")
	public static WebElement QOLCFChk;

	@FindBy(name = "qolExtremeDiff")
	public static WebElement QOLEnvAllChk;
	
	@FindBy(name = "homeCountry")
	public static WebElement EACHomoCityLst;

	@FindBy(xpath = "//p[contains(text(),'Spendable Income Details')]//ancestor::td/table")
	public static WebElement SIHHNResultTbl;

	@FindBy(xpath = "//td[text()='Cost of Living Allowance Input Details']/../../..")
	public static WebElement COLAInputTbl;
	
	@FindBy(xpath = "//td[text()='Cost of Living Allowance Results']/../../..")
	public static WebElement COLAResultsTbl;
	
	@FindBy(xpath = "//a[contains(text(),'Country/Market')]//ancestor::table")
	public static WebElement ERICResultTbl;
	
	@FindBy(xpath = "//p[contains(text(),'Base Country')]//ancestor::table")
	public static WebElement ISTRessultTbl;
	
	@FindBy(xpath = "//span[contains(text(),'Gross Annual Base Salary')]//ancestor::table")
	public static WebElement EACResultTbl;
	
	@FindBy(xpath = "//td[text()='Base City']//ancestor::table")
	public static WebElement QOLResultTbl;
	
	@FindBy(id = "chkSaveRecords")
	public static WebElement ECCSaveInputChk;
	
	@FindBy(id = "//table[@class = 'wmmForm']")
	public static WebElement ECCResultTbl;

	public GHRMCalculatorsPage() {
		PageFactory.initElements(driver, this);
	}

	public void clickCalculatorsLink(String linkName) {
		WebElement ele = driver.findElement(By.xpath("//span[contains(text(),'" + linkName + "')]"));
		clickElement(ele);
	}

	public void selectBaseCountryLocation(String baseCountry, String baseCity) {
		selEleByVisbleText(baseCountryLst, baseCountry);
		selEleByVisbleText(baseCityLst, baseCity);
	}

	public void selectHostCountryLocation(String hostCountry, String hostCity) {
		selEleByVisbleText(hostCountryLst, hostCountry);
		selEleByVisbleText(hostCityLst, hostCity);
	}

	public void selectCOLLocations(String baseCountry, String baseCity, String hostCountry, String hostCity) {
		selectBaseCountryLocation(baseCountry, baseCity);
		selectHostCountryLocation(hostCountry, hostCity);
	}

	public void enterSIHMCalculatorDetails(String country, String AnnaulSal) {
		selEleByVisbleText(SIHHCountryLst, country);
		setInput(SIHHAnnualSalTxt, AnnaulSal);
	}

	public void clickCalculateBtn() {
		clickElement(calculateBtn);
	}

	public void enterCOLADetails(String annaulSal, String baseCountry, String baseCity, String hostCountry,
			String hostCity) {
		setInput(COLAAnnualSalTxt, annaulSal);
		selectBaseCountryLocation(baseCountry, baseCity);
		selectHostCountryLocation(hostCountry, hostCity);
	}

	public void clickViewExchangeRateBtn() {
		clickElement(viewexchangeRateRdo);
	}

	public void selectISIDetails(String baseCountry, String baseCity) {
		selEleByVisbleText(ISTBaseCountryLst, baseCountry);
		selEleByVisbleText(baseCityLst, baseCity);
	}

	public void selectEACCDetails(String hostCountry, String hostCity, String homeCity) {
		switchToWindow("Housing Own Table calculator");
		selectHostCountryLocation(hostCountry, hostCity);
		selEleByVisbleText(EACHomoCityLst, homeCity);
	}

	public void enterQOLDetails(String baseCountry, String baseCity, String hostCountry, String hostCity) {
		selectBaseCountryLocation(baseCountry, baseCity);
		selectHostCountryLocation(hostCountry, hostCity);
		clickElement(QOLTravelChk);
		clickElement(QOLCFChk);
		clickElement(QOLEnvAllChk);
	}

	public void enterECCDetails(String annaulSal, String baseCountry, String baseCity, String hostCountry,
			String hostCity) {
		setInput(COLAAnnualSalTxt, annaulSal);
		selectBaseCountryLocation(baseCountry, baseCity);
		selectHostCountryLocation(hostCountry, hostCity);
		clickElement(ECCSaveInputChk);
	}
	
	public void clickCreateOwnTableBtn() {
		clickElement(EACCCreateOwnTableBtn);
	}

}
