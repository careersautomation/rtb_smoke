package pages.GHRM_Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;

import utilities.InitTests;
import static utilities.DateUtils.getCurrTimeStamp;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.selEleByVisbleText;
import java.util.List;
import static utilities.SikuliActions.*;

public class GHRMReportsPage extends InitTests {

	public static String fileName = "";

	@FindBy(name = "basket")
	public static WebElement basketLst;

	@FindBy(name = "sDate")
	public static WebElement surveyDateLst;

	@FindBy(name = "country")
	public static WebElement countryLst;

	@FindBy(name = "countrySelect")
	public static WebElement countryMultLst;
	
	@FindBy(name = "Select2")
	public static WebElement regionLst;
	
	@FindBy(name = "city")
	public static WebElement cityLst;

	@FindBy(name = "Submit")
	public static WebElement viewReportsBtn;

	@FindBy(xpath = "//a[contains(text(),'Next')] ")
	public static WebElement nextLnk;

	@FindBy(xpath = "//a[contains(text(),'View')]")
	public static WebElement viewReportLnk;

	public GHRMReportsPage() {
		super("GHMV");
		PageFactory.initElements(driver, this);
	}

	public void clickReportsLink(String link) {
		WebElement ele = driver.findElement(By.xpath("//a[contains(text(),'" + link + "')]"));
		clickElement(ele);
	}

	public void selectReportRegionAndCountry(String country, String city) {
		selEleByVisbleText(countryLst, country);
		selEleByVisbleText(cityLst, city);
	}

	public void clickViewResultBtn() {
		clickElement(viewReportsBtn);
	}

	public void clickViewReportsLink() {
		clickElement(viewReportLnk);
	}

	public void clickReportFromTable(String link) throws InterruptedException {
	Thread.sleep(3000);
		WebElement ele = driver.findElement(By.xpath("//a[contains(text(),'" + link + "')]"));
		clickElement(ele);
	}
	
	public void selectCountryFromMutliLst(String country) {
		selEleByVisbleText(countryMultLst, country);
	}

	public String checkCOLResultTableContainsValue() {
		String result = "false";
		int i = 0;
		outerloop: do {
			clickElement(nextLnk);
			List<WebElement> elements = driver
					.findElements(By.xpath("//table[@class='wmmRepHeading']//following::table[@cellpadding='0']"));
			for (WebElement element : elements) {
				List<WebElement> tdElements = element.findElements(By.tagName("//td"));
				for (WebElement ele : tdElements) {
					try {
						String value = ele.getText();
						int val = Integer.parseInt(value);
						result = "true";
						break outerloop;
					} catch (NumberFormatException e) {
						result = "false";
					}
				}
			}

		} while (i < 5);

		return result;
	}

	public void savePDFReport(String reportName) throws FindFailed, InterruptedException {
		String date = getCurrTimeStamp();
		fileName = "Libraries\\Documents\\" + fileName + reportName + "_" + date + ".pdf";
		if (BROWSER_TYPE.equalsIgnoreCase("chrome")) {
			saveFileOpenedInBrowser(reportName, BROWSER_TYPE);
		} else if (BROWSER_TYPE.equalsIgnoreCase("ie")) {
			doubleClick("OpenBtn");
			saveFileOpenedInBrowser(reportName, BROWSER_TYPE);
		}

	}

}
