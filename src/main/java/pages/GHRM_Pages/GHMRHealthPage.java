package pages.GHRM_Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.driver;

public class GHMRHealthPage {

	@FindBy(xpath = "//h2[text()='Global HRMonitor Health Check']")
	public static WebElement GHMRHealthTitleElm;
	
	public GHMRHealthPage() {
		PageFactory.initElements(driver, this);
	}
	
}
