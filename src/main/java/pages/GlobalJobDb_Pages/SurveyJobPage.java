package pages.GlobalJobDb_Pages;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class SurveyJobPage {

	@FindBy (xpath = "//table[contains(@id,'PageMenu_3')]//tr[1]//td[2]/nobr[contains(text(),'Admin')]")
	public static WebElement Admin;

	@FindBy (xpath = "//table[contains(@id,'PageMenu_3_3')]//tr[1]//td[2]/nobr[contains(text(),'Survey')]")
	public static WebElement SurveyLink;

	@FindBy (xpath = "//table[contains(@id,'PageMenu_3_3_2')]//tr[1]//td[2]/nobr[contains(text(),'Search Survey')]")
	public static WebElement SearchSurvey;

	@FindBy (id = "MultiSelect1_btnShowAll")
	public static WebElement ShowAll_Btn;

	@FindBy (xpath = "//td[@class=' WebGridTableCell ']/..//td[@title='APL01']")
	public static WebElement excel_Data;
	
	public void menuHovering(WebElement Parentelement, WebElement childElement,WebElement childElements) {
		hoverOverElement(driver,Parentelement);
		waitForElementToEnable(childElement);
		hoverOverElement(driver,childElement);
		hoverOverElement(driver,childElements);
		clickElement(childElements);
	}
	
	
	public void btnClick(WebElement element) {
		clickElement(element);
		waitForPageLoad();
	}

	public boolean checkPresenceOfElement(WebElement element ,int time ) {

		Boolean Text = isElementExisting(driver,element, time);
		return Text;
	}

	public SurveyJobPage() {
		PageFactory.initElements(driver, this);
	}
}
