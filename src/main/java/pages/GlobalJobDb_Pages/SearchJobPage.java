package pages.GlobalJobDb_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class SearchJobPage {
	
	/*@FindBy (xpath = "//*[@id=\'Head1\']/title")
	public static WebElement HomepageTitle ;
	*/
	@FindBy (xpath = "//table[@id='Menu1uwmPageMenu_2']/tbody/tr/td[2]/nobr")
	public static WebElement JobHeader;
	
	@FindBy (xpath = "//table[@id=\'Menu1uwmPageMenu_2_3\']//tr[1]//td[2]/nobr[contains(text(), 'Search Job')]")
	public static WebElement JobHeader1;
	
	@FindBy (xpath = "//*[@id='MultiSelect1_ddlSearchCriteria']")
	public static WebElement jobsDropdown;
	
	@FindBy (xpath = "//*[@id=\'MultiSelect1_ddlSearchOtions']")
	public static WebElement jobsDropdown2;
	
	@FindBy (xpath = "//*[@id='MultiSelect1_txtSearchValue1']")
	public static WebElement jobsDropdown3;
	
	@FindBy (xpath = "//*[@id='MultiSelect1_btnGo']")
	public static WebElement goButton;
	
	@FindBy (xpath = "//*[@id='lnkbtnExport']")
	public static WebElement exportExcel;
	
	@FindBy (xpath = "//tr//th[@id='UltWebGridSearchResult_c_0_3'][@class='ig_f1dc5748_r4 articleFilter bodyText maincontenttable_td']")
	public static WebElement jobFamilyName;
	
	
	@FindBy (xpath = "//td[@level = '0_3'][contains(@class, 'WebGridTableCell')]//nobr")
	public static WebElement excelData;
	
	
	public void menuHovering(WebElement Parentelement, WebElement childElement) {
		hoverOverElement(driver,Parentelement);
		waitForElementToEnable(childElement);
		hoverOverElement(driver,childElement);
		clickElement(childElement);
	}
	
	public void selectFromDropdown(WebElement locator, String option) {
		
		waitForElementToEnable(locator);
		selEleByVisbleText(locator, option);
	}
	
	public void enterInput(WebElement element , String expected) {
		
		setInput(element, expected);
	}
	
	public void btnClick(WebElement element) {
		clickElement(element);
		waitForPageLoad(); 
	}
	
	
	public SearchJobPage() {
		PageFactory.initElements(driver, this);
	}

}
