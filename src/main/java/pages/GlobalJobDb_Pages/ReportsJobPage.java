package pages.GlobalJobDb_Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

public class ReportsJobPage {
	
	@FindBy (xpath = "//table[@id='Menu1uwmPageMenu_1']//tr[1]/td[2]/nobr[contains(text() , 'Home')]")
	public static WebElement Home;

	@FindBy (xpath = "//table[@id='Menu1uwmPageMenu_4']//tr[1]/td[2]/nobr[contains(text() , 'Report')]")
	public static WebElement Report_Navbar;
	
	@FindBy (xpath = "//table[@id='Menu1uwmPageMenu_4_1']//tr[1]//td[2]/nobr[contains(text(),'Job Family')]")
	public static WebElement JobFamily_Navbar;
	
	@FindBy (id = "MultiSelect1_btnShowAll")
	public static WebElement ShowAll_Btn;
	
	@FindBy (id = "lbtExportToExcel")
	public static WebElement Excel_Export;
	
	@FindBy (xpath = "//table[contains(@id,'Menu1uwmPageMenu_3')]//tr[1]//td[2]/nobr[contains(text(),'Admin')]")
	public static WebElement Admin;
	
	@FindBy (xpath = "//table[contains(@id,'Menu1uwmPageMenu_3_1')]//tr[1]//td[2]/nobr[contains(text(),'Job Family')]")
	public static WebElement JobFamily_Admin;
	
	@FindBy (xpath = "//table[contains(@id,'Menu1uwmPageMenu_3_1_2')]//tr[1]//td[2]/nobr[contains(text(),'Search Job Family')]")
	public static WebElement SearchJobFamily;
	
	/*@FindBy (xpath = "//div(@class='ig_dad5c66d_r0 bodyText ')")
	public static WebElement DataTable;*/
	
	@FindBy (xpath = "//td[@level='0_1'][contains(@class, ' WebGridTableCell ')]//nobr")
	public static WebElement assert_reports;
	
	public boolean checkPresenceOfElement(WebElement element ,int time ) {
		
		Boolean Text = isElementExisting(driver, element, time);
		return Text;
	}
	
	
	public void menuHovering(WebElement Parentelement, WebElement childElement) {
		hoverOverElement(driver,Parentelement);
		waitForElementToEnable(childElement);
		hoverOverElement(driver,childElement);
		clickElement(childElement);
	}
	
	public void menuHovering(WebElement Parentelement, WebElement childElement,WebElement childElements) {
		waitForElementToDisplay(Parentelement);
		hoverOverElement(driver,Parentelement);
		waitForElementToEnable(childElement);
		hoverOverElement(driver,childElement);
		hoverOverElement(driver,childElements);
		clickElement(childElements);
	}
	
	public void btnClick(WebElement element) {
		waitForElementToDisplay(element);
		clickElementUsingJavaScript(driver, element);
		waitForPageLoad();
	}
	
	public void delete_Cookies() {
		deleteCookies(driver);
	}
	public ReportsJobPage() {
		PageFactory.initElements(driver, this);
	}
	

}