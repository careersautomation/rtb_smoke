package pages.GlobalJobDb_Pages;


import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Homepage {

	@FindBy(css="td.HomepageText #Text1")
	public static WebElement Header;
	
	public Homepage() {
		PageFactory.initElements(driver, this);
	}

}
