package pages.HB3x;

import static driverfactory.Driver.setInput;
import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import driverfactory.Driver;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.*;
	public class Script_pages {
		 
		
		@FindBy(xpath = "//*[@id=\"username\"]")
	    public static WebElement UsernameTextBox;
	 
	    @FindBy(xpath = "//*[@id=\"password\"]")
	    WebElement PasswordTextBox;
	    
	    @FindBy(xpath = "/html/body/mas-app/ng-component/mas-template-loader/div/ng-component/div/div[3]/mas-page/ng-component/ng-component/mas-template-loader/div/ng-component/div/div/div[2]/mas-login/div/form/mas-login-action/div/button")
	    public static WebElement LoginButton;
	   
	    
	    @FindBy(xpath = "//*[@id=\"HW\"]")
	    public static WebElement Health_Welfare;
	    
	    @FindBy(xpath = "//*[@id=\"Personal Data\"]")
	    public static WebElement My_Personal_Data;
	    
	    @FindBy(xpath = "/html/body/table/tbody/tr[2]/td[3]/table[3]/tbody/tr/td/a[2]/img")
	    public static WebElement Change_Button;
	    
	    @FindBy(name="txtAddress1")
	    WebElement Address1_column;
	    
	    @FindBy(name="txtAddress2")
	    WebElement Address2_column;
	    
	    @FindBy(name="txtAddress3")
	    WebElement Province_column;
	    
	    @FindBy(name = "txtPhone")
	    WebElement HomePhone_column;
	    
	    @FindBy(name="imgSubmit")
	    WebElement SubmitButton_MPD;
	    
	    
	    //@FindBy(xpath = "//*[@id=\"*lK36*kK7232*x1*t1542365612557\"]/div[1]/div/div[2]/div[1]/table/tbody/tr/td[2]/div")
	    //public static WebElement Verify_Mercer_Standard_policy_page;
	    
	    @FindBy(xpath = "/html/body/table/tbody/tr[2]/td[3]/table[3]/tbody/tr/td/a[1]/img")
	    public static WebElement Continue_MPD;
	    
	    @FindBy(xpath = "//*[@id=\"Beneficiary Data\"]")
	    public static WebElement My_Beneficiary;
	  
	    @FindBy(xpath = "//*[@id=\"Current Coverage\"]")
	    public static WebElement My_Current_Coverage;
	    
	    @FindBy(xpath = "/html/body/table/tbody/tr[2]/td[3]/table[3]/tbody/tr/td/a/img")
	    WebElement Continue_MCC;
	    
	    @FindBy(xpath = "/html/body/table/tbody/tr[1]/td/table[1]/tbody/tr/td[2]/a[3]")
	    WebElement Logout;
	    
	    //@FindBy(xpath = "//*[contains(text(),'Manage My Pushpins')]")
	   // public static WebElement Verify_MAP_tab;
	   /* 
	    @FindBy(xpath = "//*[contains(text(),'General Info')]")
	    WebElement GeneralInfo_tab;
	    
	    @FindBy(xpath = "//*[contains(text(),'General Info')]")
	    public static WebElement Verify_GeneralInfo_tab;
	    
	    @FindBy(xpath = "//*[contains(text(),'Home')]")
	    WebElement Home_tab;
	    
	    @FindBy(xpath = "//*[contains(text(),' Sign Out')]")
	    WebElement logout;
	    
	    @FindBy(xpath = "//div[@id='divLoggedOutConfirm']/div[2]/div/div[1]")
	    public static WebElement Verify_logout;
	  */
	    
	    
	 
	    
	   public Script_pages() {
		 PageFactory.initElements(driver, this);
	 }
	 
		
		
	 /*public void Mercer_Mobility_folder_Click() throws InterruptedException {
		 waitForElementToDisplay(Mercer_Mobility_folder);
		clickElement(Mercer_Mobility_folder);
		Thread.sleep(1000);
		
	 }*/
	 
	 public void Login_Functionality_Click(String username,String password) throws InterruptedException {
		// waitForElementToDisplay(UserIdTextBox);
		 setInput(UsernameTextBox, username  );
		 Thread.sleep(2000);
		 setInput(PasswordTextBox, password  );
		// waitForElementToDisplay(SubmitButton);
		 //clickElement(SubmitButton);
		 Thread.sleep(2000);
		 clickElement(LoginButton);
		 }
	/* public void PasswordTextBox_Click(String password) throws InterruptedException {
		
			Thread.sleep(1000);
			
		 }*/
	 /*public void LoginButton_Click() throws InterruptedException {
		 //waitForElementToDisplay(Housing_Folder);
			
			
			
		 }
	 */
	 public void Health_Welfare_Click() throws InterruptedException {
		 //waitForElementToDisplay(Housing_Folder);
		 
			clickElement(Health_Welfare);
			
			Thread.sleep(2000);
			
		 }
	 public void My_Personal_Data_Click() throws InterruptedException {
		 waitForElementToDisplay(My_Personal_Data);
		 clickElement(My_Personal_Data);
		 waitForElementToDisplay(Change_Button);
		 clickElement(Change_Button);
			
		 }
	 
	 public void My_Personal_DataChange_Click() throws InterruptedException {
		 Address1_column.clear();
		 setInput(Address1_column, "Return to sender"  );
		 Address2_column.clear();
		 setInput(Address2_column, "Address2"  );
		 Province_column.clear();
		 setInput(Province_column, "Province"  );
		 HomePhone_column.clear();
		 setInput(HomePhone_column, "8795641230"  );
		 Thread.sleep(1000);
		 clickElement(SubmitButton_MPD);
		 waitForElementToDisplay(Change_Button);
		 clickElement(Continue_MPD);
		 Thread.sleep(2000);
			
		 }
	 
	 public void My_Current_Coverage_Click() throws InterruptedException {
		 waitForElementToDisplay(My_Current_Coverage);
		 clickElement(My_Current_Coverage);
		 waitForElementToDisplay(Continue_MCC);
		 clickElement(Continue_MCC);
		 }
	
	 public void My_Beneficiary() throws InterruptedException {
		clickElement(My_Beneficiary);
		Thread.sleep(2000);
		driver.navigate().back();;
		Thread.sleep(1000);
	 
	 }
	 
	 public void Logout_Click() throws InterruptedException {
		 waitForElementToDisplay(Logout);
		 clickElement(Logout);
		 Thread.sleep(1000);
			
		 }
	 /*public void ResetAllSelector_Button_Click() throws InterruptedException {
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		 js.executeScript("scroll(1024,0);", ResetAllSelector_Button);
		 //clickElement(ResetAllSelector_Button);
		 
			
		 }
	 public void MAP_tab_Click() throws InterruptedException {
		 clickElement(MAP_tab);
		 waitForElementToDisplay(Verify_MAP_tab);
			
		 }
	 public void GeneralInfo_tab_Click() throws InterruptedException {
		 waitForElementToDisplay(GeneralInfo_tab);
		 clickElement(GeneralInfo_tab);
		 Thread.sleep(1000);
			
		 }
	 
	 public void Home_tab_Click() throws InterruptedException {
		 waitForElementToDisplay(Home_tab);
		 clickElement(Home_tab);
		 Thread.sleep(1000);
			
		 }
	 
	 public void logout_Click() throws InterruptedException {
		 clickElement(logout);
		 Thread.sleep(1000);
			
		 }
	 
	 public void VerifyElementTitle() throws InterruptedException {
		 String actualTitle = driver.getTitle();
		 String expectedTitle = "";
		 assertEquals(expectedTitle,actualTitle);
		 Thread.sleep(1000);
			
		 }*/
	 
	}