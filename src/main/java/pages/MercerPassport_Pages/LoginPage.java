package pages.MercerPassport_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	@FindBy (xpath = "//table[@id = 'Table7']//tr[1]/td")
	public static WebElement welcomeMessage;

	@FindBy (xpath = "//input[@name='userEmail']")
	public static WebElement email_Input;
	
	@FindBy (xpath = "//input[@name='userPassword']")
	public static WebElement password_Input;
	
	@FindBy (xpath = "//input[@name='authenticateUser']")
	public static WebElement login_Button;
	
	@FindBy(xpath = "//a[@id='overridelink']")
	public static WebElement accpetCertificate;
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void mp_login(String username, String password) {
		int count =(driver.findElements(By.xpath("//a[@id='overridelink']"))).size();
		if(count>0) {
			
			clickElement(accpetCertificate);
		}
	
	
		setInput(email_Input, username);
		setInput(password_Input, password);
		clickElement(login_Button);
	}
	

}
