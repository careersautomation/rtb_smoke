package pages.MercerPassport_Pages;

import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderPage {

	@FindBy(xpath = "//a[@class='headerLink'][contains(text(),'Home')]")
	public static WebElement home_Button;

	@FindBy(xpath = "//a[@class='headerLink'][contains(text(),'Logout')]")
	public static WebElement logout_Button;

	@FindBy(xpath = "//div[@class = 'menu-item'][contains(text(), 'Settling In')]")
	public static WebElement setlingIn_Menu;

	@FindBy(xpath = "//a[@class = 'daddy'][contains(text(), 'Money and Banking')]")
	public static WebElement monerAndBank_Button;

	@FindBy(xpath = "//a[contains(text(), 'Cost Advisor')]")
	public static WebElement costAdvisor_Button;

	@FindBy(xpath = "//a[contains(text(), 'Conversion')]")
	public static WebElement conversion_Button;

	@FindBy(xpath = "//ul[@id='nav2']/li/div")
	public static List<WebElement> headercontent;

	public HeaderPage() {
		PageFactory.initElements(driver, this);
	}

	public void navigatingViaMenu(WebElement headerMenu, WebElement mainMenu) {
//		hoverOverElement(driver,headerMenu);
//		clickElementUsingJavaScript(driver,mainMenu);
		hoverAndClickOnElement(driver, headerMenu, mainMenu);

	}

	public void navigatingViaMenu(WebElement headerMenu, WebElement mainMenu, WebElement subMenu) {
		hoverOverElement(driver,headerMenu);
//		hoverAndClickOnElement(driver, headerMenu, mainMenu);
		hoverOverElement(driver,mainMenu);
//		clickElementUsingJavaScript(driver,subMenu);
		clickElement(subMenu);

	}

	public void navigateToHome() {
		clickElement(home_Button);
		HomePage homePage = new HomePage();
		waitForElementToDisplay(HomePage.country_Dropdown);
		System.out.println("Country dropdown present on screen");
	}

	public List<WebElement> getHeader() {
		return (headercontent);
	}

	public void mp_logout() {

		clickElement(logout_Button);

	}
}
