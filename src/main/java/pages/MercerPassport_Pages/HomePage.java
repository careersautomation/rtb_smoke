package pages.MercerPassport_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.selEleByVisbleText;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class HomePage {


	@FindBy(xpath = "//select[@name='countryCode']")
	public static WebElement country_Dropdown;
	
	@FindBy (id = "load")
	public static WebElement selectCountryMarket;
	
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void selectCountry (String value) {
		selEleByVisbleText(country_Dropdown, value);
		
	}
	
	public void clickCountryMarket() {
		clickElement(selectCountryMarket);
	}
	
	public List<WebElement> fetchHeaderMenuElement() {
		HeaderPage headerPage = new HeaderPage();		
		return(headerPage.getHeader());
	}
		
}
