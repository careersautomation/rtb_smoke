package pages.MercerPassport_Pages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.clickElement;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.waitForElementToDisplay;

public class CostAdvisorPage {
	
	@FindBy(xpath = "//td[@class='wmmBigTitle'][contains(text(), 'CostAdvisor')]")
	public static WebElement costAdvisor_Title;
	
	@FindBy(id = "Select3")
	public static WebElement coyntryB_Dropdown;
	
	@FindBy(xpath = "//select[@id='availableOptions']/option[@value='1']")
	public static WebElement option1;
	
	@FindBy(xpath = "//select[@id='availableOptions']/option[@value='2']")
	public static WebElement option2;
	
	@FindBy(xpath = "//input[@type='button'][@value = '  -->  ']")
	public static WebElement moveToSelected_Button;
	
	@FindBy(id = "Submit1")
	public static WebElement calculate_Button;
	
	@FindBy(xpath = "//input[@name='chkExportExcel']")
	public static WebElement exportToExcel_checkbox;
	
	@FindBy(xpath = "//td[@class = 'wmmBigTitleC']")
	public static WebElement headerCityResults;
	
	
	public CostAdvisorPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	
	public void selectCountry (String value) {
		selEleByVisbleText(coyntryB_Dropdown,  value);
		}
	
	public void selectOptions(WebElement e) {
		clickElement(e);
		clickElement(moveToSelected_Button);
	}
}
