package pages.MercerPassport_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UnitConversionPage {
	
	@FindBy(xpath="//h1[contains(text(), 'General Conversion Tables')]")
	public static WebElement conversionPagetitle;
	
	
	@FindBy (xpath = "//a[@href='ConversionCharts.pdf']")
	public static WebElement printVersion_Button;
	
	
	public UnitConversionPage() {
		PageFactory.initElements(driver, this);
	}

}
