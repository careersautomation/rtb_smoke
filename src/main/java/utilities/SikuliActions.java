package utilities;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import static driverfactory.Driver.driver;

public class SikuliActions {
	public static Screen screen;
	public static Pattern pattern;
	public static String screenPath = "/src/main/resources/WindowScreens";

	public SikuliActions() {
		screen = new Screen();
	}

	public static void doubleClick(String screenName) throws FindFailed {
		screenPath = screenPath + "/" + screenName+".png";
		System.out.println(screenPath);
		pattern = new Pattern(screenPath);
		screen.doubleClick(screenPath);
		screen.wait(pattern, 10);
	}

	public static void setFilePath(String screenName, String path) throws FindFailed {
		screenPath = screenPath + "/" + screenName+".png";
		pattern = new Pattern(screenPath);
		screen.type(pattern, path);
		screen.wait(pattern, 10);
	}

	public static void click(String screenName) throws FindFailed {
		screenPath = screenPath + "/" + screenName;
		pattern = new Pattern(screenPath);
		screen.click(pattern);
		screen.wait(pattern, 10);
	}

	public static void saveFileOpenedInBrowser(String fileName, String browserType) throws FindFailed, InterruptedException {
		if(browserType.equalsIgnoreCase("ie")) 
			click("SaveImg");
		Actions builder = new Actions(driver);
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.chord("s")).build().perform();
		Thread.sleep(2000);
		doubleClick("FileNameTxt");
		setFilePath("FileNameTxt", fileName);
		click("SaveBtn");
		setFilePath("FileNameTxt", fileName);
		click("SaveBtn");
		
		}
	}
